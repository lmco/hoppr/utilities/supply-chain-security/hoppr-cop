---
workflow:
  rules:
    # Filters out chore releases
    - if: $CI_COMMIT_MESSAGE =~ /^chore\(release\).*/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      variables:
        CI_COMMIT_BRANCH: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG
      when: never
    - when: always

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  # - template: Security/Container-Scanning.gitlab-ci.yml

  - local: .gitlab/ci/*.yml

variables:
  # Global SAST Variables
  SAST_EXCLUDED_PATHS: spec,test,tests,tmp,node_modules,target,venv
  SAST_BANDIT_EXCLUDED_PATHS: "*/test/**,*/tests/**,*/node_modules/**,*/target/**,*/venv/**"

  SECRET_DETECTION_EXCLUDED_PATHS: docs

  PYTHON_IMAGE: docker.io/library/python
  PYTHON_TAG: "3.10"

  ## Semantic Release
  ENABLE_SEMANTIC_RELEASE: "true"
  ENABLE_SEMANTIC_RELEASE_DRY_RUN: "true"

  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"

default:
  artifacts:
    name: $CI_JOB_ID
    paths:
      - ${ARTIFACT_NAME}-metadata.json
  tags:
    - saas-linux-large-amd64

stages:
  - pre
  - build
  - check
  - test
  - docs
  - deploy

download-binaries:
  stage: pre
  image: $PYTHON_IMAGE:$PYTHON_TAG
  script:
    # Download cosign
    - curl --silent --fail --show-error --location
      --url https://github.com/sigstore/cosign/releases/latest/download/cosign-linux-amd64
      --output cosign && chmod +x $_

    # Download cyclonedx-cli
    - curl --silent --fail --show-error --location
      --url https://github.com/CycloneDX/cyclonedx-cli/releases/latest/download/cyclonedx-linux-x64
      --output cyclonedx-cli && chmod +x $_

    # Download witness
    - curl --silent --fail --show-error --location
      --url https://raw.github.com/in-toto/witness/main/install-witness.sh | bash
    - mv /usr/local/bin/witness .
  cache:
    key: binaries-$CI_COMMIT_SHORT_SHA
    policy: push
    paths:
      - cosign
      - cyclonedx-cli
      - witness

sbom:
  stage: docs
  image: $PYTHON_IMAGE:$PYTHON_TAG
  needs:
    - job: download-binaries
  variables:
    WITNESS_OPTIONS: >
      --enable-archivista
      --outfile sbom-attestation.json
      --signer-fulcio-token $SIGSTORE_ID_TOKEN
      --step sbom-generation
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore
  script:
    - pip install cyclonedx-bom
    - rm sbom.json

    - |
      if [ "$CI_COMMIT_REF_NAME" != "main" ] || [ -z ${RELEASE_VERSION} ]; then
        cyclonedx-py poetry -o sbom.json --of json
      else
        ./witness run $WITNESS_OPTIONS -- cyclonedx-py poetry -o sbom.json --of json
        ./cosign sign-blob -y sbom.json --bundle cosign.bundle
      fi
  artifacts:
    paths:
      - sbom*.json
      - "*.bundle"
  cache:
    key: binaries-$CI_COMMIT_SHORT_SHA
    policy: pull
    paths:
      - cosign
      - witness

gemnasium-python-dependency_scanning:
  stage: check
  needs: []

semgrep-sast:
  stage: check
  needs: []

secret_detection:
  stage: check
  needs: []
