"""HopprCop test using the plugin as entrypoint."""
from __future__ import annotations

from copy import deepcopy
from pathlib import Path
from threading import _RLock as RLock
from unittest import TestCase

from hoppr import HopprContext, Manifest, Sbom
from packageurl import PackageURL

from hopprcop import __version__
from hopprcop.hoppr_plugin.hopprcop_plugin import HopprCopPlugin
from hopprcop.ossindex.oss_index_scanner import OSSIndexScanner


class TestHopprCopPlugin(TestCase):
    """Run hopprcop plugin to test hoppr-cop functionality."""

    manifest = Manifest.load(Path("test") / "integration" / "hoppr" / "manifest.yml")

    simple_test_context = HopprContext(
        repositories=manifest.repositories,
        collect_root_dir=Path("COLLECTION_DIR"),
        consolidated_sbom=manifest.consolidated_sbom,
        credential_required_services=None,
        delivered_sbom=deepcopy(manifest.consolidated_sbom),
        retry_wait_seconds=1,
        max_processes=3,
        sboms=list(Sbom.loaded_sboms.values()),
        stages=[],
        logfile_lock=RLock(),
    )

    simple_config = {}  # noqa: RUF012

    def test_get_version(self):
        """Test the hopprcop plugin get_version method."""
        assert HopprCopPlugin.get_version(self) == __version__

    def test_pre_stage_process_success(self):
        """Test the hopprcop plugin pre_stage_process method."""
        hoppr50 = HopprCopPlugin(self.simple_test_context, self.simple_config)
        result = hoppr50.pre_stage_process()

        assert result.is_success()


class TestOSSScanner(TestCase):
    """Test OSS Index Scanner."""

    def test_convert_cwe(self):
        """Test OSS Index Scanner get_vulnerabilities_by_purl method."""
        scanner = OSSIndexScanner()
        scanner.api.oss_index_authentication = None

        purl = PackageURL(type="pypi", name="ruamel.yaml", version="0.17.21")

        vuln = scanner.get_vulnerabilities_by_purl([purl])

        assert vuln
