from __future__ import annotations

import shutil
import zipfile

from pathlib import Path
from typing import TYPE_CHECKING, TypeAlias

import pytest
import requests
import rich

from hoppr import Vulnerability, cdx
from packageurl import PackageURL

from hopprcop.gemnasium.gemnasium_scanner import GemnasiumScanner  # noqa: TC001
from hopprcop.vulnerability_scanner import VulnerabilitySuper


if TYPE_CHECKING:
    from hopprcop.gemnasium.models import GemnasiumVulnerability


FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="scanner")
def gemnasium_scanner_fixture(scanner: GemnasiumScanner, resources_dir: Path) -> GemnasiumScanner:
    scanner.database_path = resources_dir / "gemnasium" / "vuln_files"
    return scanner


@pytest.fixture(name="db_path")
def database_path_fixture(request: FixtureRequest, tmp_path: Path) -> Path:
    update = getattr(request, "param", True)
    return (tmp_path / Path("hoppr-demos-main.zip").stem) if update else tmp_path


@pytest.fixture(name="semver_path")
def semver_path_fixture(request: FixtureRequest, resources_dir: Path) -> Path:
    test_file = getattr(request, "param", "gemnasium")
    return resources_dir / test_file


@pytest.mark.parametrize(
    argnames=["expected", "super_activate", "download_db", "offline", "semver_path"],
    argvalues=[
        pytest.param(
            True,
            True,
            True,
            False,
            "gemnasium",
            id="simple-activate",
        ),
        pytest.param(
            False,
            False,
            True,
            False,
            "gemnasium",
            id="not-super-activate",
        ),
        pytest.param(
            True,
            True,
            True,
            False,
            "doesnotexist",
            id="no-semver-path-activate",
        ),
        pytest.param(
            False,
            True,
            False,
            True,
            "gemnasium",
            id="not-and-offline-activate",
        ),
    ],
    indirect=["semver_path"],
)
def test_should_activate(
    expected: bool,
    super_activate: bool,
    download_db: bool,
    offline: bool,
    semver_path: Path,
    scanner: GemnasiumScanner,
    monkeypatch: MonkeyPatch,
):
    monkeypatch.undo()
    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="required_tools_on_path", value=[])
        patch.setattr(target=scanner, name="offline_mode", value=offline)
        patch.setattr(target=scanner, name="semver_path", value=semver_path)
        patch.setattr(target=scanner, name="_extract_semver_to_local", value=lambda: True)
        patch.setattr(target=VulnerabilitySuper, name="should_activate", value=lambda _: super_activate)
        patch.setattr(target=scanner, name="_download_and_extract_database", value=lambda: download_db)

        actual = scanner.should_activate()

        assert actual == expected


def test__convert_to_cyclone_dx():
    ...


@pytest.mark.parametrize(
    argnames=["expected"],
    argvalues=[
        pytest.param(
            True,
            id="simple-downloadandextract",
        ),
    ],
)
def test__do_download_and_unpack(
    expected: bool,
    scanner: GemnasiumScanner,
    tmp_path: Path,
    resources_dir: Path,
    monkeypatch: MonkeyPatch,
):
    def _get_patch(url: str | bytes, allow_redirects: bool) -> requests.Response:
        zip_text_path = tmp_path / "zipfile-test.txt"
        zip_text_path.write_bytes(b"fake" * 1024)
        zip_file_path = tmp_path / "zipfile-test.zip"

        with zipfile.ZipFile(file=zip_file_path, mode="x") as zip_file:
            zip_file.write(filename=zip_text_path, arcname="fake-zip/zipfile-test.txt")

        response = requests.Response()
        response._content = zip_file_path.read_bytes()
        return response

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="database_path", value=tmp_path)
        patch.setattr(target=scanner, name="url", value="https://fake-url/fake.zip")
        patch.setattr(target=requests, name="get", value=_get_patch)

        scanner._do_download_and_unpack(tmp_path / "fake.zip")

        assert (tmp_path / "fake.zip").exists() == expected
        assert (tmp_path / "fake-zip" / "zipfile-test.txt").exists() == expected


@pytest.mark.parametrize(
    argnames=["expected", "expected_out", "db_path", "zip_exists", "offline", "older"],
    argvalues=[
        pytest.param(
            False,
            "GemnasiumScanner: Unable to find local database file ",
            False,
            False,
            True,
            False,
            id="notexists-offline-newer-db",
        ),
        pytest.param(
            True,
            "Download and Unpack",
            True,
            False,
            False,
            False,
            id="notexists-online-newer-db",
        ),
        pytest.param(
            True,
            "GemnasiumScanner: Database file is up to date",
            True,
            True,
            False,
            False,
            id="exists-online-newer-db",
        ),
        pytest.param(
            True,
            "Download and Unpack",
            True,
            True,
            False,
            True,
            id="exists-online-older-db",
        ),
        pytest.param(
            True,
            "GemnasiumScanner: Database file is up to date",
            True,
            True,
            True,
            False,
            id="exists-offline-newer-db",
        ),
        pytest.param(
            True,
            "GemnasiumScanner: Database file is older than one day for offline mode",
            True,
            True,
            True,
            True,
            id="exists-offline-older-db",
        ),
    ],
    indirect=["db_path"],
)
def test__download_and_extract_database(
    expected: bool,
    expected_out: str,
    db_path: Path,
    zip_exists: bool,
    offline: bool,
    older: bool,
    scanner: GemnasiumScanner,
    tmp_path: Path,
    capsys: pytest.CaptureFixture[str],
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="database_path", value=tmp_path)
        patch.setattr(target=scanner, name="url", value="https://fake-url/hoppr-demos-main.zip")
        patch.setattr(target=scanner, name="_get_cache_dir", value=lambda: tmp_path)
        patch.setattr(target=scanner, name="_do_download_and_unpack", value=lambda _: rich.print("Download and Unpack"))
        patch.setattr(target=scanner, name="_older_than", value=lambda *_: older)
        patch.setattr(target=scanner, name="offline_mode", value=offline)
        patch.setattr(target=Path, name="exists", value=lambda _: zip_exists)
        patch.setattr(target=shutil, name="rmtree", value=lambda _: True)

        actual = scanner._download_and_extract_database()

        out, error = capsys.readouterr()
        output_lines = list(filter(None, out.split("\n")))

        assert actual == expected
        assert scanner.database_path == db_path
        assert expected_out in output_lines


def test__extract_semver_to_local():
    ...


@pytest.mark.parametrize(
    argnames=["expected", "directory"],
    argvalues=[
        pytest.param(
            Path("test"),
            "test",
            id="simple-cache-path",
        ),
        pytest.param(
            Path("really/long/path/with/a/bunch/of/directories/"),
            "really/long/path/with/a/bunch/of/directories/",
            id="long-cache-path",
        ),
        pytest.param(
            Path(
                "reallybigdirectorynamethatshouldnotevenbereasonabletohavebuttestinganywayjusttobesurethateverythingworks"
            ),
            "reallybigdirectorynamethatshouldnotevenbereasonabletohavebuttestinganywayjusttobesurethateverythingworks",
            id="longdirname-cache-path",
        ),
    ],
)
def test__get_cache_dir(expected: Path, directory: str, scanner: GemnasiumScanner, monkeypatch: MonkeyPatch):
    with monkeypatch.context() as patch:
        patch.setenv("CACHE_DIR", directory)

        actual = scanner._get_cache_dir()

        assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "purl"],
    argvalues=[
        pytest.param(
            Path("maven/not.a.real/artifact"),
            PackageURL.from_string("pkg:maven/not.a.real/artifact@1.0.1"),
            id="maven-get-path",
        ),
        pytest.param(
            Path("npm/not.a.real/artifact"),
            PackageURL.from_string("pkg:npm/not.a.real/artifact@1.0.1"),
            id="npm-get-path",
        ),
        pytest.param(
            Path("go/not.a.real/artifact"),
            PackageURL.from_string("pkg:golang/not.a.real/artifact@1.0.1"),
            id="golang-get-path",
        ),
        pytest.param(
            Path("pypi/artifact"),
            PackageURL.from_string("pkg:pypi/not.a.real/artifact@1.0.1"),
            id="pypi-get-path",
        ),
    ],
)
def test__get_path(expected: Path, purl: PackageURL, scanner: GemnasiumScanner, monkeypatch: MonkeyPatch):
    actual = scanner._get_path(purl)

    assert actual == scanner.database_path / expected


def test__is_affected_range():
    ...


@pytest.mark.parametrize(
    argnames=["expected", "purls", "ratings", "in_range"],
    argvalues=[
        pytest.param(
            [
                "CVE-2018-10237",
                "CVE-2020-8908",
                "CVE-2023-2976",
            ],
            ["pkg:maven/com.google.guava/guava@16.0.1"],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            True,
            id="basic-vulnbypurl",
        ),
        pytest.param(
            [
                "CVE-2018-10237",
                "CVE-2020-8908",
                "CVE-2023-2976",
                "CVE-2017-1000487",
                "GHSA-g6ph-x5wf-g337",
                "GHSA-jcwr-x25h-x5fh",
                "CVE-2017-18640",
                "CVE-2022-1471",
                "CVE-2022-25857",
                "CVE-2022-38749",
                "CVE-2022-38750",
                "CVE-2022-38751",
                "CVE-2022-38752",
                "CVE-2022-41854",
            ],
            [
                "pkg:maven/com.google.guava/guava@16.0.1",
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20",
                "pkg:maven/org.yaml/snakeyaml@1.33",
            ],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            True,
            id="multiple-purls-vulnbypurl",
        ),
        pytest.param(
            [],
            ["pkg:maven/com.google.guava/guava@16.0.1"],
            [],
            True,
            id="no-rating-vulnbypurl",
        ),
        pytest.param(
            [],
            [
                "pkg:maven/com.google.guava/guava@16.0.1",
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20",
                "pkg:maven/org.yaml/snakeyaml@1.33",
            ],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            False,
            id="not-in-range-vulnbypurl",
        ),
        pytest.param(
            [],
            ["pkg:maven/not.a.real/package@1.0.0"],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            False,
            id="not-file-vulnbypurl",
        ),
    ],
    indirect=["ratings"],
)
def test_get_vulnerabilities_for_purl(
    expected: list[str],
    purls: list[str],
    in_range: bool,
    scanner: GemnasiumScanner,
    ratings: list[cdx.Rating] | None,
    monkeypatch: MonkeyPatch,
):
    def get_path_patch(purl: PackageURL) -> Path:
        if purl.namespace:
            return scanner.database_path / purl.type / purl.namespace / purl.name
        return scanner.database_path / purl.type / purl.name

    def is_affected_range_patch(repository_format: str, version: str, affected_range: str) -> bool:
        return in_range

    def convert_to_cyclone_dx_patch(vuln: GemnasiumVulnerability) -> Vulnerability:
        return Vulnerability(id=vuln.identifiers[0], ratings=ratings)

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="_get_path", value=get_path_patch)
        patch.setattr(
            target=scanner,
            name="_is_affected_range",
            value=is_affected_range_patch,
        )
        patch.setattr(
            target=scanner,
            name="_convert_to_cyclone_dx",
            value=convert_to_cyclone_dx_patch,
        )

        actual = scanner.get_vulnerabilities_for_purl(purls)

        assert len(actual) == len(expected)
        actual_vulns = [vuln.id for vuln in actual]
        actual_vulns.sort()
        expected.sort()
        assert actual_vulns == expected


@pytest.mark.parametrize(
    argnames=["expected", "purls", "in_range"],
    argvalues=[
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "CVE-2018-10237",
                    "CVE-2020-8908",
                    "CVE-2023-2976",
                ]
            },
            [PackageURL.from_string("pkg:maven/com.google.guava/guava@16.0.1")],
            True,
            id="basic-vulnbypurl",
        ),
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "CVE-2018-10237",
                    "CVE-2020-8908",
                    "CVE-2023-2976",
                ],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": [
                    "CVE-2017-1000487",
                    "GHSA-g6ph-x5wf-g337",
                    "GHSA-jcwr-x25h-x5fh",
                ],
                "pkg:maven/org.yaml/snakeyaml@1.33": [
                    "CVE-2017-18640",
                    "CVE-2022-1471",
                    "CVE-2022-25857",
                    "CVE-2022-38749",
                    "CVE-2022-38750",
                    "CVE-2022-38751",
                    "CVE-2022-38752",
                    "CVE-2022-41854",
                ],
            },
            [
                PackageURL.from_string("pkg:maven/com.google.guava/guava@16.0.1"),
                PackageURL.from_string("pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20"),
                PackageURL.from_string("pkg:maven/org.yaml/snakeyaml@1.33"),
            ],
            True,
            id="multiple-purls-vulnbypurl",
        ),
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": [],
                "pkg:maven/org.yaml/snakeyaml@1.33": [],
            },
            [
                PackageURL.from_string("pkg:maven/com.google.guava/guava@16.0.1"),
                PackageURL.from_string("pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20"),
                PackageURL.from_string("pkg:maven/org.yaml/snakeyaml@1.33"),
            ],
            False,
            id="not-in-range-vulnbypurl",
        ),
        pytest.param(
            {
                "pkg:maven/not.a.real/package@1.0.0": [],
            },
            [
                PackageURL.from_string("pkg:maven/not.a.real/package@1.0.0"),
            ],
            False,
            id="not-file-vulnbypurl",
        ),
    ],
)
def test_get_vulnerabilities_by_purl(
    expected: dict[str, list[str]],
    purls: list[PackageURL],
    in_range: bool,
    scanner: GemnasiumScanner,
    ratings: list[cdx.Rating] | None,
    monkeypatch: MonkeyPatch,
):
    def get_path_patch(purl: PackageURL) -> Path:
        if purl.namespace:
            return scanner.database_path / purl.type / purl.namespace / purl.name
        return scanner.database_path / purl.type / purl.name

    def is_affected_range_patch(repository_format: str, version: str, affected_range: str) -> bool:
        return in_range

    def convert_to_cyclone_dx_patch(vuln: GemnasiumVulnerability) -> Vulnerability:
        return Vulnerability(id=vuln.identifiers[0], ratings=ratings)

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="_get_path", value=get_path_patch)
        patch.setattr(
            target=scanner,
            name="_is_affected_range",
            value=is_affected_range_patch,
        )
        patch.setattr(
            target=scanner,
            name="_convert_to_cyclone_dx",
            value=convert_to_cyclone_dx_patch,
        )

        actual = scanner.get_vulnerabilities_by_purl(purls)

        assert len(actual) == len(expected)
        for purl in expected:
            actual_vulns = [vuln.id for vuln in actual[purl]]
            actual_vulns.sort()
            expected[purl].sort()
            assert actual_vulns == expected[purl]
