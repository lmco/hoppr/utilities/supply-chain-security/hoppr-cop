from __future__ import annotations

from threading import _RLock as RLock
from typing import TYPE_CHECKING, TypeAlias

import pytest

from hoppr import HopprContext, Manifest, Sbom, Vulnerability

import hopprcop.hoppr_plugin.hopprcop_plugin

from hopprcop.hoppr_plugin.hopprcop_plugin import HopprCopPlugin


if TYPE_CHECKING:
    from pathlib import Path


MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="plugin")
def plugin_fixture(resources_dir: Path, sbom: Sbom, tmp_path: Path) -> HopprCopPlugin:
    manifest = Manifest.load(resources_dir.parent / "integration" / "hoppr" / "manifest.yml")

    return HopprCopPlugin(
        context=HopprContext(
            repositories=manifest.repositories,
            collect_root_dir=tmp_path,
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=sbom.copy(deep=True),
            max_processes=10,
            sboms=[sbom],
            stages=[],
            logfile_lock=RLock(),
        )
    )


def test__add_bom_ref_and_flatten():
    ...


def test__perform_hoppr_bom_updates():
    ...


def test__perform_hopprcop_reporting():
    ...


def test_get_version():
    ...


@pytest.mark.parametrize(
    argnames=["expected_vulns", "expected_affects", "vulnerabilities", "affects"],
    argvalues=[
        pytest.param(
            1,
            3,
            ["CVE-1234-5678"],
            [
                {"ref": "pkg:maven/com.google.guava/guava@16.0.1"},
                {"ref": "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20"},
                {"ref": "pkg:maven/org.yaml/snakeyaml@1.33"},
            ],
            id="single_plugin_run",
        ),
        pytest.param(
            2,
            3,
            ["CVE-1234-5678", "CVE-1357-8903"],
            [
                {"ref": "pkg:maven/com.google.guava/guava@16.0.1"},
                {"ref": "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20"},
                {"ref": "pkg:maven/org.yaml/snakeyaml@1.33"},
            ],
            id="multi_plugin_run",
        ),
    ],
    indirect=["vulnerabilities", "affects"],
)
def test_pre_stage_process(
    expected_vulns: int,
    expected_affects: int,
    vulnerabilities: list[Vulnerability],
    plugin: HopprCopPlugin,
    monkeypatch: MonkeyPatch,
):
    def generate_vulnerability_reports_patch(_, __, vulnerabilities: list[Vulnerability], bom: Sbom):
        bom.vulnerabilities = vulnerabilities

    monkeypatch.setattr(
        target=hopprcop.hoppr_plugin.hopprcop_plugin.CombinedScanner,  # type: ignore[attr-defined]
        name="get_vulnerabilities_for_sbom",
        value=lambda *_, **__: vulnerabilities,
    )
    monkeypatch.setattr(
        target=hopprcop.hoppr_plugin.hopprcop_plugin.Reporting,  # type: ignore[attr-defined]
        name="generate_vulnerability_reports",
        value=generate_vulnerability_reports_patch,
    )

    result = plugin.pre_stage_process()

    assert isinstance(result.return_obj, Sbom)
    assert result.return_obj.vulnerabilities
    assert len(result.return_obj.vulnerabilities) == expected_vulns
    assert len(result.return_obj.vulnerabilities[0].affects) == expected_affects
