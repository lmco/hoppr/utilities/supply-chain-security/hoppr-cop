from __future__ import annotations

from typing import TYPE_CHECKING, TypeAlias

import pytest
import rich

from hoppr import Vulnerability  # noqa: TC002
from requests import Response

import hopprcop

from hopprcop.enhancements.epss_enhancer import EpssEnhancer


if TYPE_CHECKING:
    from pathlib import Path

FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="response")
def response_fixture(request: FixtureRequest, resources_dir: Path) -> Response:
    """Test fixture to return a parametrized requests.Response."""
    param_dict = dict(getattr(request, "param", {"content": "unit_epss_single_cve_result.json", "status_code": 200}))
    file_path = resources_dir / "enhancements/epss/" / param_dict.get("content", "unit_epss_single_cve_result.json")

    response = Response()
    response.status_code = param_dict.get("status_code", 200)
    response._content = file_path.read_bytes()  # pylint: disable=protected-access
    response.raw = response.content

    return response


@pytest.mark.parametrize(
    argnames=["expected", "vulnerabilities", "chunk"],
    argvalues=[
        pytest.param(
            ["CVE-2023-48795"],
            ["CVE-2023-48795"],
            10,
            id="basic-enhance-vulns",
        ),
        pytest.param(
            ["CVE-2024-12345,CVE-2023-12345,GHSA-1980-12345,TEMP-12903123"],
            ["CVE-2024-12345", "CVE-2023-12345", "GHSA-1980-12345", "TEMP-12903123"],
            10,
            id="multi-enhance-vulns",
        ),
        pytest.param(
            ["CVE-2024-12345,CVE-2023-12345", "GHSA-1980-12345,TEMP-12903123"],
            ["CVE-2024-12345", "CVE-2023-12345", "GHSA-1980-12345", "TEMP-12903123"],
            2,
            id="two-chunk-enhance-vulns",
        ),
        pytest.param(
            [],
            [],
            10,
            id="no-vulns-enhance-vulns",
        ),
    ],
    indirect=["vulnerabilities"],
)
def test_enhance_vulnerabilities(
    expected: list[str],
    vulnerabilities: list[Vulnerability],
    chunk: int,
    enhancer: EpssEnhancer,
    capsys: pytest.CaptureFixture[str],
    monkeypatch: MonkeyPatch,
):
    def query_epss_api_patch(_, chunk_str: str, data: dict[str, str]):
        data[chunk_str[:2]] = chunk_str

    def set_epss_score_patch(_, __, data: dict[str, str]):
        for value in data.values():
            rich.print(value)

    with monkeypatch.context() as patch:
        patch.setattr(target=EpssEnhancer, name="query_epss_api", value=query_epss_api_patch)
        patch.setattr(target=EpssEnhancer, name="set_epss_score", value=set_epss_score_patch)

        enhancer.chunk_size = chunk
        enhancer.enhance_vulnerabilities(vulnerabilities)
        out, error = capsys.readouterr()
        output_lines = list(filter(None, out.split("\n")))
        for exp in expected:
            assert exp in output_lines


@pytest.mark.parametrize(
    argnames=["expected", "data_results", "response"],
    argvalues=[
        pytest.param(
            {"CVE-2023-48795": {"epss": "0.694740000", "percentile": "0.979280000"}},
            {},
            {"content": "unit_epss_single_cve_result.json", "status_code": 200},
            id="basic-query-epss-api",
        ),
    ],
    indirect=["response"],
)
def test_query_epss_api(
    expected: dict[str, dict[str, str]],
    data_results: dict[str, dict[str, str]],
    response: Response,
    enhancer: EpssEnhancer,
    monkeypatch: MonkeyPatch,
):
    def api_query_patch(data: str) -> Response:
        return response

    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.enhancements.epss_enhancer, name="api_query", value=api_query_patch)

        enhancer.query_epss_api("", data_results)

        assert data_results == expected


@pytest.mark.parametrize(
    argnames=["expected", "vulnerabilities", "data_results"],
    argvalues=[
        pytest.param(
            {
                "CVE-2024-29041": {"epss": "0.000440000", "percentile": "0.088330000"},
                "CVE-2024-28849": {"epss": "0.000440000", "percentile": "0.088330000"},
                "CVE-2024-28246": {"epss": "0.000430000", "percentile": "0.075850000"},
                "CVE-2024-28245": {"epss": "0.000430000", "percentile": "0.075850000"},
            },
            [],
            {
                "CVE-2024-29041": {"epss": "0.000440000", "percentile": "0.088330000"},
                "CVE-2024-28849": {"epss": "0.000440000", "percentile": "0.088330000"},
                "CVE-2024-28246": {"epss": "0.000430000", "percentile": "0.075850000"},
                "CVE-2024-28245": {"epss": "0.000430000", "percentile": "0.075850000"},
            },
            id="basic-set-epss-score",
        ),
    ],
)
def test_set_epss_score(
    expected: dict[str, dict[str, str]],
    vulnerabilities: list[Vulnerability],
    data_results: dict[str, dict[str, str]],
    enhancer: EpssEnhancer,
    monkeypatch: MonkeyPatch,
):
    enhancer.set_epss_score(vulnerabilities, data_results)

    assert data_results == expected
