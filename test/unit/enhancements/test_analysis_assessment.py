from __future__ import annotations

from typing import TYPE_CHECKING, TypeAlias

import pytest
import rich

from hoppr import Affect, Vulnerability, cdx
from packageurl import PackageURL

from hopprcop.enhancements.analysis_assessment import Analysis


if TYPE_CHECKING:
    from pathlib import Path

FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="assessment_obj")
def assessment_obj_fixture(assessment: Analysis) -> Analysis.Assessment:
    return assessment.assessments[2]


@pytest.fixture(name="assessment_package")
def assessment_package_fixture(assessment_obj: Analysis.Assessment) -> Analysis.Assessment.Package:
    return assessment_obj.package


@pytest.fixture
def average_severity_fixture() -> str:
    return "medium"


@pytest.fixture
def analysis_fixture() -> Analysis:
    return Analysis()


@pytest.fixture(name="assessment")
def assessment_fixture(resources_combined_path_fixture: Path, analysis_fixture: Analysis) -> Analysis:
    analysis_fixture._set_assessment_path(resources_combined_path_fixture)

    return analysis_fixture


@pytest.fixture
def path_fixture(request: FixtureRequest, resources_dir: Path) -> str | Path:
    param_dict = dict(getattr(request, "param", {}))
    condition = param_dict.get("condition", 1)
    directory = param_dict.get("directory", "")

    if condition == 1:
        return f"{resources_dir.absolute()}/{directory}"
    elif condition == 2:
        return resources_dir.absolute() / directory
    else:
        return directory


@pytest.fixture
def assessment_init_fixture(request: FixtureRequest) -> dict:
    param_dict = dict(getattr(request, "param", {}))
    # key to omit, tests required fields
    target_key = param_dict.get("target_key", "")
    target_package_key = param_dict.get("target_package_key", "")

    # Validate Keys
    keys = ["assessmentDate", "vulnerability", "state", "package", "justification", "response", "detail"]

    package: dict = {}
    assessment_dict: dict = {"package": package}

    for key in keys:
        if key != target_key or target_key == "package":
            match key:
                case "assessmentDate":
                    assessment_dict[key] = "2023-10-11"
                case "detail":
                    assessment_dict[key] = "See CVE, the vulnerability applicable if application"
                case "justification":
                    if target_key == "justification:invalid":
                        assessment_dict[key] = "invalid_justification"
                    else:
                        assessment_dict[key] = "code_not_reachable"
                case "package":
                    if target_package_key != "type":
                        package["type"] = "npm"
                    if target_package_key != "name":
                        package["name"] = "semver"
                    if target_package_key != "version":
                        package["version"] = "7.3.8"
                    if target_package_key == "version:invalid":
                        package["version"] = ">=7.3.8"
                case "response":
                    if target_key == "response:invalid":
                        assessment_dict[key] = "invalid_response"
                    else:
                        assessment_dict[key] = "can_not_fix"
                case "state":
                    assessment_dict[key] = "false_positive"
                case "vulnerability":
                    assessment_dict[key] = "CVE-2021-3918"

    return assessment_dict


@pytest.fixture
def purl_data_fixture() -> PackageURL:
    return PackageURL("npm", None, "semver", "7.3.8", {}, None)


@pytest.fixture
def resources_combined_path_fixture(resources_dir: Path) -> Path:
    return resources_dir.absolute() / "combined"


@pytest.fixture
def scanner_name_fixture() -> str:
    return "GrypeScanner"


@pytest.fixture
def scanner_results_fixture(vulnerability: Vulnerability) -> dict[str, list[Vulnerability]]:
    result: dict[str, list[Vulnerability]] = {}
    for affect in vulnerability.affects:
        result.setdefault(affect.ref, [])
        result[affect.ref].append(vulnerability)
    return result


@pytest.mark.parametrize(
    argnames="path_fixture, expected",
    argvalues=[
        pytest.param(
            {"condition": 1, "directory": "combined"},
            True,
            id="valid-path-by-string",
        ),
        pytest.param(
            {"condition": 2, "directory": "combined"},
            True,
            id="valid-path-by-path",
        ),
        pytest.param(
            {"condition": 3, "directory": "combined"},
            False,
            id="valid-path-by-invalid",
        ),
    ],
    indirect=["path_fixture"],
)
def test__set_assessment_path(path_fixture: str | Path, expected: bool, analysis_fixture: Analysis):
    result = analysis_fixture._set_assessment_path(path_fixture)
    assert result is expected


@pytest.mark.parametrize(
    argnames="assessment_init_fixture, expected",
    argvalues=[
        pytest.param(
            {"target_key": "assessmentDate"},
            "assessments.assessmentDate must the date the assessment conducted",
            id="missing_assessmentDate",
        ),
        pytest.param(
            {"target_key": "vulnerability"},
            "assessments.vulnerability must contain a vulnerability id",
            id="missing_vulnerability",
        ),
        pytest.param(
            {"target_key": "state"},
            "assessments.state must be one of",
            id="missing_state",
        ),
        pytest.param(
            {"target_key": "package", "target_package_key": "type"},
            "assessments.package.type",
            id="missing_package_type",
        ),
        pytest.param(
            {"target_key": "package", "target_package_key": "name"},
            "assessments.package.name",
            id="missing_package_name",
        ),
        pytest.param(
            {"target_key": "package", "target_package_key": "version"},
            "assessments.package.version",
            id="missing_package_version",
        ),
        pytest.param(
            {"target_key": "justification"},
            "assessments.justification",
            id="missing_justification",
        ),
        pytest.param(
            {"target_key": "justification:invalid"},
            "assessments.justification",
            id="missing_justification_invalid",
        ),
        pytest.param(
            {"target_key": "response:invalid"},
            "assessments.response",
            id="missing_response_invalid",
        ),
        pytest.param(
            {"target_key": "detail"},
            "assessments.detail",
            id="missing_detail",
        ),
    ],
    indirect=["assessment_init_fixture"],
)
def test_assessment__init__(assessment_init_fixture: dict, expected: str, analysis_fixture: Analysis):
    with pytest.raises(KeyError) as error:
        analysis_fixture.Assessment(assessment_init_fixture)
    assert expected in str(error.value)


@pytest.mark.parametrize(
    argnames=["expected_strings", "vulnerabilities"],
    argvalues=[
        pytest.param(
            ["CVE-2022-25883 and CVE-2022-25883"],
            ["CVE-2022-25883"],
            id="basic-apply-assessments",
        ),
    ],
    indirect=["vulnerabilities"],
)
def test_apply_assessments(
    expected_strings: list[str],
    vulnerabilities: list[Vulnerability],
    assessment: Analysis,
    capsys: pytest.CaptureFixture[str],
    monkeypatch: MonkeyPatch,
):
    def determine_assessment_patch(assessment: Analysis.Assessment, vuln: Vulnerability):
        rich.print(f"{assessment.vulnerability} and {vuln.id}")

    with monkeypatch.context() as patch:
        patch.setattr(target=assessment, name="_determine_assessment", value=determine_assessment_patch)

        assessment.apply_assessments(vulnerabilities)

        out, error = capsys.readouterr()
        output_lines = list(filter(None, out.split("\n")))

        for expected in [
            f"{assessment.vulnerability} and {vuln.id}"
            for assessment in assessment.assessments
            for vuln in vulnerabilities
        ]:
            assert expected in output_lines


@pytest.mark.parametrize(
    argnames=["affected", "vulnerability"],
    argvalues=[
        pytest.param(
            True,
            {"id": "CVE-2022-25883"},
            id="basic-determine-assessment",
        ),
    ],
    indirect=["vulnerability"],
)
def test__determine_assessment(
    affected: bool,
    assessment_obj: Analysis.Assessment,
    vulnerability: Vulnerability,
    assessment: Analysis,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=assessment, name="_package_is_affected_by_vuln", value=lambda *_: affected)

        assessment._determine_assessment(assessment_obj, vulnerability)

        for rating in vulnerability.ratings or []:
            assert rating.severity == cdx.Severity.low
        assert vulnerability.analysis
        assert vulnerability.analysis.state == "false_positive"
        assert vulnerability.analysis.justification == "code_not_present"


@pytest.mark.parametrize(
    argnames=["expected", "affects"],
    argvalues=[
        pytest.param(
            True,
            [{"ref": "pkg:npm/semver@7.3.8"}],
            id="basic-package-affected",
        ),
        pytest.param(
            False,
            [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}],
            id="not-package-affected",
        ),
        pytest.param(
            True,
            [{"ref": "pkg:npm/semver@7.3.8"}, {"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}],
            id="multi-package-affected",
        ),
    ],
    indirect=["affects"],
)
def test__package_is_affected_by_vuln(
    expected: bool,
    assessment_package: Analysis.Assessment.Package,
    affects: list[Affect],
    assessment: Analysis,
):
    actual = assessment._package_is_affected_by_vuln(assessment_package, affects)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["vulnerability", "affects"],
    argvalues=[
        pytest.param(
            {"id": "CVE-2022-25883"},
            [{"ref": "pkg:npm/semver@7.3.8"}],
            id="basic-check-assessments",
        ),
    ],
    indirect=["vulnerability", "affects"],
)
def test_check_assessments(
    scanner_results_fixture: dict[str, list[Vulnerability]],
    scanner_name_fixture: str,
    analysis_fixture: Analysis,
    assessment: Analysis.Assessment,
):
    result = analysis_fixture.check_assessments(scanner_results_fixture, scanner_name_fixture)

    if result["pkg:npm/semver@7.3.8"][0].ratings:
        assert str(result["pkg:npm/semver@7.3.8"][0].ratings[0].severity) == "low"


@pytest.mark.parametrize(
    argnames=["vulnerability", "ratings"],
    argvalues=[
        pytest.param(
            {"id": "CVE-2022-25883"},
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                },
                {
                    "score": "5.9",
                    "severity": "high",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                },
            ],
            id="basic-check-assessment",
        ),
    ],
    indirect=["vulnerability", "ratings"],
)
def test__check_assessment(
    purl_data_fixture: PackageURL,
    assessment_obj: Analysis.Assessment,
    vulnerability: Vulnerability,
    assessment: Analysis,
):
    vuln_array = [vulnerability]
    assessment._check_assessment(purl_data_fixture, assessment_obj, vuln_array)

    if vulnerability.ratings:
        assert str(vulnerability.ratings[0].severity) == "low"


@pytest.mark.parametrize(
    argnames=["vulnerability", "ratings"],
    argvalues=[
        pytest.param(
            {"id": "CVE-2022-25883"},
            [
                {
                    "severity": "medium",
                    "score": 5.3,
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:L",
                }
            ],
            id="basic-check_rating",
        ),
    ],
    indirect=["vulnerability", "ratings"],
)
def test__check_rating(
    assessment_obj: Analysis.Assessment,
    average_severity_fixture: str,
    vulnerability: Vulnerability,
    analysis_fixture: Analysis,
):
    rating = vulnerability.ratings[0] if vulnerability.ratings else cdx.Rating()
    analysis_fixture._check_rating(assessment_obj, average_severity_fixture, vulnerability, rating)
    assert str(rating.severity) == "low"


@pytest.mark.parametrize(
    argnames=["ratings"],
    argvalues=[
        pytest.param(
            [{"severity": "low"}, {"severity": "medium"}, {"severity": "high"}],
            id="basic-get-average-severity",
        ),
    ],
    indirect=["ratings"],
)
def test__get_average_severity(ratings: list[cdx.Rating], analysis_fixture: Analysis):
    average_severity = analysis_fixture._get_average_severity(ratings)
    assert average_severity == "medium"
