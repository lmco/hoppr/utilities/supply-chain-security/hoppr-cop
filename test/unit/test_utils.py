"""Unit tests for utils.py."""

from __future__ import annotations

import pytest

from hoppr import Vulnerability
from packageurl import PackageURL

from hopprcop.utils import _add_vulnerability, purl_check, unsupported_purl_feedback


@pytest.mark.parametrize(
    argnames=["expected", "first_purl", "second_purl"],
    argvalues=[
        pytest.param(
            True,
            "pkg:npm/qs@6.5.2",
            "pkg:npm/qs@6.5.2",
            id="basic-purl-check",
        ),
        pytest.param(
            False,
            "pkg:npm/qs@6.5.2",
            "pkg:npm/qs@6.7.0",
            id="versions-purl-check",
        ),
        pytest.param(
            False,
            "pkg:maven/qs@6.5.2",
            "pkg:npm/qs@6.5.2",
            id="type-purl-check",
        ),
        pytest.param(
            False,
            "pkg:npm/qs@6.5.2",
            "pkg:npm/test/qs@6.5.2",
            id="namespace-purl-check",
        ),
        pytest.param(
            False,
            "pkg:npm/not-test/qs@6.5.2",
            "pkg:npm/test/qs@6.5.2",
            id="diff-namespace-purl-check",
        ),
        pytest.param(
            True,
            "pkg:npm/qs@6.5.2",
            "pkg:npm/qs@6.5.2?qual=test",
            id="one-quals-purl-check",
        ),
        pytest.param(
            True,
            "pkg:npm/qs@6.5.2?qual=test",
            "pkg:npm/qs@6.5.2?qual=test",
            id="both-quals-purl-check",
        ),
        pytest.param(
            True,
            "pkg:npm/qs@6.5.2?qual=test%2Ffuzzing",
            "pkg:npm/qs@6.5.2?qual=test/fuzzing",
            id="fuzzy-quals-purl-check",
        ),
    ],
)
def test_purl_check(
    expected: bool,
    first_purl: str,
    second_purl: str,
):
    actual = purl_check(first_purl, second_purl)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["vuln_list", "vulnerability"],
    argvalues=[
        pytest.param(
            {},
            {"id": "CVE-2022-25883"},
            id="basic-add-vulnerability",
        ),
        pytest.param(
            {"CVE-2022-25883": Vulnerability(id="CVE-2022-25883")},
            {"id": "CVE-2022-25883"},
            id="exists-add-vulnerability",
        ),
        pytest.param(
            {
                "CVE-2022-25883": Vulnerability(id="CVE-2022-25883"),
                "CVE-2022-23432": Vulnerability(id="CVE-2022-23432"),
            },
            {"id": "CVE-2022-25883"},
            id="multiple-entries-add-vulnerability",
        ),
    ],
    indirect=["vulnerability"],
)
def test__add_vulnerability(
    vuln_list: dict[str, Vulnerability],
    vulnerability: Vulnerability,
):
    key = vulnerability.id or "Unknown"
    _add_vulnerability(key, vuln_list, vulnerability)

    assert key in vuln_list
    combined = vuln_list[key]
    assert combined.id == vulnerability.id
    for rating in vulnerability.ratings or []:
        assert combined.ratings
        assert rating in combined.ratings
    for affect in vulnerability.affects or []:
        assert combined.affects
        assert affect in combined.affects


@pytest.mark.parametrize(
    argnames=["supported_types", "purl_list", "expected_print"],
    argvalues=[
        pytest.param(
            ["conan", "gem", "golang", "maven", "npm", "nuget", "pypi"],
            [PackageURL.from_string("pkg:deb/com.google.guava/guava@16.0.1")],
            "WARNING: test -- does not support purls of type deb, components may be missed.\n",
            id="supported purls",
        ),
        pytest.param(
            ["conan", "gem", "golang", "maven", "npm", "nuget", "pypi"],
            [
                PackageURL.from_string("pkg:maven/com.google.guava/guava@16.0.1"),
                PackageURL.from_string("pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20"),
                PackageURL.from_string("pkg:maven/org.yaml/snakeyaml@1.33"),
            ],
            "",
            id="supported purls",
        ),
    ],
)
def test_unsupported_purl_feedback(
    capsys,  # noqa: ANN001
    supported_types: list[str],
    purl_list: list[PackageURL],
    expected_print: str,
):
    unsupported_purl_feedback("test", supported_types, purl_list)
    capturedoutput = capsys.readouterr()
    assert capturedoutput.out == expected_print
