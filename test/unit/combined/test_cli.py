from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING, TypeAlias

import pytest

import hopprcop.combined.cli as cli
import hopprcop.vulnerability_scanner


if TYPE_CHECKING:
    from typer.testing import CliRunner


MonkeyPatch: TypeAlias = pytest.MonkeyPatch


def test_vulnerability_report_exception(
    hoppr_cop_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    def _get_vulnerabilities_for_sbom_patch(*_, **__):
        raise ValueError("pytest exception")

    with monkeypatch.context() as patch:
        patch.setattr(
            target=cli.CombinedScanner,  # type: ignore[attr-defined]
            name="get_vulnerabilities_for_sbom",
            value=_get_vulnerabilities_for_sbom_patch,
        )

        result = hoppr_cop_runner.invoke(
            app=cli.app,
            args=[
                "--trace",
                "--format=html",
                "--format=table",
                "--format=cyclone_dx",
                f"--output-dir={tmp_path}",
                f"{resources_dir / 'bom' / 'unit_syft_java_bom.json'}",
            ],
            catch_exceptions=False,
        )

        assert result.exit_code == 1

        output_lines = list(filter(None, result.stdout.split("\n")))
        assert "unexpected error: pytest exception" in output_lines


def test_version(hoppr_cop_runner: CliRunner):
    result = hoppr_cop_runner.invoke(
        app=cli.app,
        args=["--version"],
        catch_exceptions=False,
    )

    assert result.exit_code == 0


@pytest.mark.parametrize(
    argnames="_should_activate_fixture",
    argvalues=[{"GemnasiumScanner": False, "GrypeScanner": False, "OSSIndexScanner": False, "TrivyScanner": False}],
    indirect=True,
)
def test_missing_format(hoppr_cop_runner: CliRunner, resources_dir: Path, tmp_path: Path, monkeypatch: MonkeyPatch):
    monkeypatch.setattr(target=cli.CombinedScanner, name="scanners", value=[])  # type: ignore[attr-defined]
    monkeypatch.setattr(
        target=hopprcop.vulnerability_scanner.distutils.spawn,  # type: ignore[attr-defined]
        name="find_executable",
        value=lambda _: None,
    )
    monkeypatch.delenv(name="OSS_INDEX_USER", raising=False)
    monkeypatch.delenv(name="OSS_INDEX_TOKEN", raising=False)

    result = hoppr_cop_runner.invoke(
        app=cli.app,
        args=[
            "--trace",
            f"--output-dir={tmp_path}",
            f"{resources_dir / 'bom' / 'unit_syft_java_bom.json'}",
        ],
        catch_exceptions=False,
    )

    assert result.exit_code == 0

    output_lines = list(filter(None, result.stdout.split("\n")))

    assert "GemnasiumScanner is not activated because required tools are not on the PATH:" in output_lines
    assert "ruby" in output_lines
    assert "GrypeScanner is not activated because required tools are not on the PATH:" in output_lines
    assert "grype" in output_lines
    assert "TrivyScanner is not activated because required tools are not on the PATH:" in output_lines
    assert "trivy" in output_lines


@pytest.mark.parametrize(
    argnames="_should_activate_fixture",
    argvalues=[{"GemnasiumScanner": False, "GrypeScanner": False, "OSSIndexScanner": False, "TrivyScanner": False}],
    indirect=True,
)
def test_missing_output_dir(hoppr_cop_runner: CliRunner, resources_dir: Path, tmp_path: Path, monkeypatch: MonkeyPatch):
    monkeypatch.chdir(tmp_path)
    monkeypatch.setattr(target=cli.CombinedScanner, name="scanners", value=[])  # type: ignore[attr-defined]

    result = hoppr_cop_runner.invoke(
        app=cli.app,
        args=[
            "--trace",
            "--format=html",
            "--format=table",
            "--format=cyclone_dx",
            f"{resources_dir / 'bom' / 'unit_syft_java_bom.json'}",
        ],
        catch_exceptions=False,
    )

    assert result.exit_code == 0
    assert Path("unit_syft_java_bom-enhanced.json").exists()


@pytest.mark.parametrize(
    argnames="_should_activate_fixture",
    argvalues=[{"GemnasiumScanner": False, "GrypeScanner": False, "OSSIndexScanner": False, "TrivyScanner": False}],
    indirect=True,
)
def test_missing_bom_file(hoppr_cop_runner: CliRunner, resources_dir: Path, monkeypatch: MonkeyPatch):
    monkeypatch.setattr(target=cli.CombinedScanner, name="scanners", value=[])  # type: ignore[attr-defined]

    result = hoppr_cop_runner.invoke(
        app=cli.app,
        args=[
            "--format=table",
            f"{resources_dir / 'bom' / 'does_not_exist.json'}",
        ],
        catch_exceptions=False,
    )

    assert result.exit_code == 1


@pytest.mark.parametrize(
    argnames="_should_activate_fixture",
    argvalues=[{"GemnasiumScanner": False, "GrypeScanner": False, "OSSIndexScanner": False, "TrivyScanner": False}],
    indirect=True,
)
def test_download_dbs_call(hoppr_cop_runner: CliRunner, monkeypatch: MonkeyPatch):
    monkeypatch.setattr(target=cli.CombinedScanner, name="scanners", value=[])  # type: ignore[attr-defined]

    monkeypatch.setattr(
        target=cli.CombinedScanner,  # type: ignore[attr-defined]
        name="get_vulnerability_dbs",
        value=lambda x: True,
    )

    result = hoppr_cop_runner.invoke(
        app=cli.app,
        args=["--download-dbs"],
        catch_exceptions=False,
    )

    assert result.exit_code == 0


@pytest.mark.parametrize(
    argnames="_should_activate_fixture",
    argvalues=[{"GemnasiumScanner": False, "GrypeScanner": False, "OSSIndexScanner": False, "TrivyScanner": False}],
    indirect=True,
)
def test_bad_cli_call(hoppr_cop_runner: CliRunner, monkeypatch: MonkeyPatch):
    monkeypatch.setattr(target=cli.CombinedScanner, name="scanners", value=[])  # type: ignore[attr-defined]

    result = hoppr_cop_runner.invoke(
        app=cli.app,
        args=[],
        catch_exceptions=False,
    )

    assert result.exit_code == 1
