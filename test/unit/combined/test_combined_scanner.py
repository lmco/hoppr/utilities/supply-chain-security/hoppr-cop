from __future__ import annotations

from typing import TYPE_CHECKING, TypeAlias

import pytest

import hopprcop.combined.combined_scanner
import hopprcop.vulnerability_combiner

from hopprcop.combined.combined_scanner import CombinedScanner, ScannerError
from hopprcop.gemnasium.gemnasium_scanner import GemnasiumScanner
from hopprcop.grype.grype_scanner import GrypeScanner
from hopprcop.ossindex.oss_index_scanner import OSSIndexScanner
from hopprcop.trivy.trivy_scanner import TrivyScanner


if TYPE_CHECKING:
    from collections.abc import Callable

    from hopprcop.vulnerability_scanner import VulnerabilitySuper


FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="combined_scanner")
def combined_scanner_fixture(request: FixtureRequest) -> CombinedScanner:
    CombinedScanner.scanners.clear()

    scanner = CombinedScanner()
    scanner.set_scanners(getattr(request, "param", []))

    return scanner


@pytest.fixture
def submit_to_scanner_fixture(
    request: FixtureRequest, monkeypatch: MonkeyPatch
) -> Callable[[VulnerabilitySuper], object]:
    param_dict = dict(getattr(request, "param", {}))
    task = param_dict.get("task", "scan")
    failure = param_dict.get("failure", [])
    raises = param_dict.get("raises", False)
    msg = param_dict.get("msg", False)

    def exc_echo(exception_msg: str):
        assert exception_msg == msg

    def _submit_to_scanner_db(scanner: VulnerabilitySuper) -> bool:
        if raises:
            raise TypeError("Test exception case")
        else:
            return type(scanner).__name__ not in failure

    def _submit_to_scanner_scan(scanner: VulnerabilitySuper) -> str:
        if raises and type(scanner).__name__ == "GemnasiumScanner":
            raise TypeError("Test exception case")
        else:
            return type(scanner).__name__

    monkeypatch.setattr(
        target=hopprcop.combined.combined_scanner.rich,  # type: ignore[attr-defined]
        name="print",
        value=exc_echo,
    )

    return _submit_to_scanner_scan if task == "scan" else _submit_to_scanner_db


@pytest.mark.parametrize(
    argnames=["expected", "combined_scanner", "submit_to_scanner_fixture"],
    argvalues=[
        pytest.param(
            {"GrypeScanner": "GrypeScanner"},
            [GrypeScanner()],
            {"task": "scan", "raises": False, "msg": ""},
            id="single-scanner",
        ),
        pytest.param(
            {"GemnasiumScanner": "GemnasiumScanner", "GrypeScanner": "GrypeScanner"},
            [GrypeScanner(), GemnasiumScanner()],
            {"task": "scan", "raises": False, "msg": ""},
            id="multi-scanner",
        ),
        pytest.param(
            {
                "GemnasiumScanner": "GemnasiumScanner",
                "GrypeScanner": "GrypeScanner",
                "OSSIndexScanner": "OSSIndexScanner",
                "TrivyScanner": "TrivyScanner",
            },
            [GrypeScanner(), GemnasiumScanner(), OSSIndexScanner(), TrivyScanner()],
            {"task": "scan", "raises": False, "msg": ""},
            id="all-scanners-by-instance",
        ),
        pytest.param(
            {
                "GemnasiumScanner": "GemnasiumScanner",
                "GrypeScanner": "GrypeScanner",
                "OSSIndexScanner": "OSSIndexScanner",
                "TrivyScanner": "TrivyScanner",
            },
            [
                "hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner",
                "hopprcop.grype.grype_scanner.GrypeScanner",
                "hopprcop.ossindex.oss_index_scanner.OSSIndexScanner",
                "hopprcop.trivy.trivy_scanner.TrivyScanner",
            ],
            {"task": "scan", "raises": False, "msg": ""},
            id="all-scanners-by-name",
        ),
        pytest.param(
            {},
            [],
            {"task": "scan", "raises": False, "msg": ""},
            id="no-scanners",
        ),
        pytest.param(
            {"GrypeScanner": True},
            [GrypeScanner()],
            {"task": "db", "failure": [], "raises": False, "msg": ""},
            id="single-succeeding-db",
        ),
        pytest.param(
            {"GrypeScanner": True, "GemnasiumScanner": False},
            [GrypeScanner(), GemnasiumScanner()],
            {"task": "db", "failure": ["GemnasiumScanner"], "raises": False, "msg": ""},
            id="single-failing-db",
        ),
        pytest.param(
            {"GrypeScanner": True, "GemnasiumScanner": True, "OSSIndexScanner": True, "TrivyScanner": True},
            [GrypeScanner(), GemnasiumScanner(), OSSIndexScanner(), TrivyScanner()],
            {"task": "db", "failure": [], "raises": False, "msg": ""},
            id="all-succeeding-byclass-db",
        ),
        pytest.param(
            {"GrypeScanner": True, "GemnasiumScanner": True, "OSSIndexScanner": True, "TrivyScanner": True},
            [
                "hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner",
                "hopprcop.grype.grype_scanner.GrypeScanner",
                "hopprcop.ossindex.oss_index_scanner.OSSIndexScanner",
                "hopprcop.trivy.trivy_scanner.TrivyScanner",
            ],
            {"task": "db", "failure": [], "raises": False, "msg": ""},
            id="all-succeeding-byname-db",
        ),
        pytest.param(
            {},
            [],
            {"task": "db", "failure": [], "raises": False, "msg": ""},
            id="zero-succeeding-db",
        ),
        pytest.param(
            {"GrypeScanner": False, "GemnasiumScanner": False},
            [GrypeScanner(), GemnasiumScanner()],
            {"task": "db", "failure": ["GrypeScanner", "GemnasiumScanner"], "raises": False, "msg": ""},
            id="all-failing-db",
        ),
    ],
    indirect=["combined_scanner", "submit_to_scanner_fixture"],
)
def test__run_concurrently(
    expected: list[str],
    monkeypatch: MonkeyPatch,
    combined_scanner: CombinedScanner,
    submit_to_scanner_fixture: Callable[[VulnerabilitySuper], str],
):
    """Test _scan_concurrently method."""
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.combined.combined_scanner, name="combine_vulnerabilities", value=set)

        actual = combined_scanner._run_concurrently(submit_to_scanner_fixture)  # type: ignore[type-var]

        assert actual == expected


@pytest.mark.parametrize(
    argnames=["combined_scanner", "submit_to_scanner_fixture"],
    argvalues=[
        pytest.param(
            [GrypeScanner(), GemnasiumScanner()],
            {"task": "scan", "raises": True, "msg": "GemnasiumScanner generated an exception: Test exception case"},
            id="partial-exception-scanner",
        ),
        pytest.param(
            [GemnasiumScanner()],
            {"task": "scan", "raises": True, "msg": "GemnasiumScanner generated an exception: Test exception case"},
            id="full-exception-scanner",
        ),
        pytest.param(
            [GrypeScanner()],
            {
                "task": "db",
                "failure": [],
                "raises": True,
                "msg": "GrypeScanner generated an exception: Test exception case",
            },
            id="single-exception-db",
        ),
        pytest.param(
            [GemnasiumScanner()],
            {
                "task": "db",
                "failure": ["GemnasiumScanner"],
                "raises": True,
                "msg": "GemnasiumScanner generated an exception: Test exception case",
            },
            id="failure-and-exception-db",
        ),
    ],
    indirect=["combined_scanner", "submit_to_scanner_fixture"],
)
def test__scan_concurrently_exception(
    monkeypatch: MonkeyPatch,
    combined_scanner: CombinedScanner,
    submit_to_scanner_fixture: Callable[[VulnerabilitySuper], str],
):
    """Test _scan_concurrently method when exception is raised."""
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.combined.combined_scanner, name="combine_vulnerabilities", value=set)

        with pytest.raises(expected_exception=ScannerError):
            combined_scanner._run_concurrently(submit_to_scanner_fixture)  # type: ignore[type-var]


@pytest.mark.parametrize(
    argnames=["expected_classes", "scanners"],
    argvalues=[
        pytest.param(
            [GrypeScanner],
            ["hopprcop.grype.grype_scanner.GrypeScanner"],
            id="set-single-scanner-by-string",
        ),
        pytest.param(
            [GemnasiumScanner, GrypeScanner, TrivyScanner],
            [
                "hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner",
                "hopprcop.grype.grype_scanner.GrypeScanner",
                "hopprcop.trivy.trivy_scanner.TrivyScanner",
            ],
            id="set-multi-scanner-by-string",
        ),
        pytest.param(
            [GrypeScanner],
            [GrypeScanner()],
            id="set-single-scanner-by-class",
        ),
        pytest.param(
            [GrypeScanner, TrivyScanner],
            [GrypeScanner(), TrivyScanner()],
            id="set-multi-scanner-by-class",
        ),
    ],
)
def test_set_scanners(expected_classes: list[type[VulnerabilitySuper]], scanners: list[VulnerabilitySuper] | list[str]):
    """Test the set_scanners method."""
    combined_scanner = hopprcop.combined.combined_scanner.CombinedScanner()

    combined_scanner.scanners.clear()

    combined_scanner.set_scanners(scanners)

    assert len(combined_scanner.scanners) == len(expected_classes)
    for scan, expected in zip(combined_scanner.scanners, expected_classes, strict=True):
        assert isinstance(scan, expected)


@pytest.mark.parametrize(
    argnames=["expected", "results", "combined_scanner"],
    argvalues=[
        pytest.param(
            True,
            {"GrypeScanner": True, "GemnasiumScanner": True, "OSSIndexScanner": True, "TrivyScanner": True},
            [GrypeScanner(), GemnasiumScanner(), OSSIndexScanner(), TrivyScanner()],
            id="all-true-db",
        ),
        pytest.param(
            False,
            {"GemnasiumScanner": True, "GrypeScanner": True, "TrivyScanner": False},
            [GemnasiumScanner(), GrypeScanner(), TrivyScanner()],
            id="one-false-db",
        ),
        pytest.param(
            False,
            {"GemnasiumScanner": False, "GrypeScanner": False, "TrivyScanner": False},
            [GemnasiumScanner(), GrypeScanner(), TrivyScanner()],
            id="three-false-db",
        ),
        pytest.param(
            False,
            {"GemnasiumScanner": True, "GrypeScanner": False, "TrivyScanner": False},
            [GemnasiumScanner(), GrypeScanner(), TrivyScanner()],
            id="two-false-db",
        ),
    ],
    indirect=["combined_scanner"],
)
def test_get_vulnerability_dbs(
    expected: bool, results: list[bool], combined_scanner: CombinedScanner, monkeypatch: MonkeyPatch
):
    """Test the get_vulnerability_dbs method."""
    with monkeypatch.context() as patch:
        patch.setattr(target=combined_scanner, name="_run_concurrently", value=lambda _: results)

        actual = combined_scanner.get_vulnerability_dbs()

        assert actual == expected
