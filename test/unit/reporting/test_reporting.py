from __future__ import annotations

import uuid

from pathlib import Path
from typing import TYPE_CHECKING, TypeAlias

import pytest
import rich

from hoppr import Affect, Component, ComponentType, Sbom, Tools, Vulnerability, cdx

import hopprcop

from hopprcop.reporting import Reporting
from hopprcop.reporting.gitlab.models import (
    CvssVectors,
    CvssVectors3x,
    Dependency,
    Identifier,
    Link,
    Location,
    NamedFieldTextItem,
    Package,
    Vulnerability as GitlabVulnerability,
)
from hopprcop.reporting.models import ReportFormat


if TYPE_CHECKING:
    from collections.abc import Mapping


FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="reporting")
def reporting_fixture(tmp_path: Path) -> Reporting:
    return Reporting(output_path=tmp_path, base_name="unit_test")


@pytest.fixture(name="linked_vulnerabilities")
def linked_vulnerabilities_fixture(
    request: FixtureRequest,
    ratings: list[cdx.Rating],
    advisories: list[cdx.Advisory],
    affects: list[Affect],
    tools: Tools,
    sbom: Sbom,
) -> list[Vulnerability]:
    param_dict = getattr(request, "param", [])

    return [
        Vulnerability(
            id=vuln,
            description="this is a test",
            recommendation="test recomendation",
            ratings=ratings,
            advisories=advisories,
            affects=[
                Affect.parse_obj(
                    {
                        "ref": f"urn:cdx:{sbom.serialNumber}/{sbom.version}#{affect.ref.__str__()}",
                        "versions": affect.versions,
                    }
                )
                for affect in affects
            ],
            tools=tools,
        )
        for vuln in param_dict
    ]


@pytest.fixture(name="purl_vuln_dict")
def purl_vuln_dict_fixture(request: FixtureRequest) -> dict[str, list[Vulnerability]]:
    param_list = getattr(request, "param", [])

    purl_vulns: dict[str, list[Vulnerability]] = {}

    for group in param_list:
        purl = group["purl"]
        purl_vulns[purl] = [
            Vulnerability(
                id=vuln["id"],
                description="This is a test",
                recommendation="test",
                ratings=vuln["ratings"],
                advisories=None,
                affects=vuln["affects"],
                tools=Tools(
                    components=[
                        Component(
                            type=ComponentType.APPLICATION,
                            supplier=cdx.OrganizationalEntity(name="Hoppr"),
                            name="HopprCop",
                            version="1.0.0",
                            scope=cdx.Scope.EXCLUDED,
                        )
                    ]
                ),
            )
            for vuln in group["vulns"]
        ]

    return purl_vulns


@pytest.fixture(name="severity_classes")
def severity_classes_fixture(tmp_path: Path) -> dict[str, str]:
    return {
        "critical": "bg-red-100 rounded-lg py-5 px-6 mb-4 text-base text-red-700 mb-3",
        "high": "bg-yellow-100 rounded-lg py-5 px-6 mb-4 text-base text-yellow-700 mb-3",
        "medium": "bg-gray-50 rounded-lg py-5 px-6 mb-4 text-base text-gray-500 mb-3",
        "info": "bg-gray-50 rounded-lg py-5 px-6 mb-4 text-base text-gray-500 mb-3",
        "low": "bg-gray-50 rounded-lg py-5 px-6 mb-4 text-base text-gray-500 mb-3",
        "unknown": "bg-gray-50 rounded-lg py-5 px-6 mb-4 text-base text-gray-500 mb-3",
        "none": "bg-gray-50 rounded-lg py-5 px-6 mb-4 text-base text-gray-500 mb-3",
    }


@pytest.mark.parametrize(
    argnames=["expected_strings", "formats", "vulnerabilities", "bom"],
    argvalues=[
        pytest.param(
            ["Generate TABLE Report"],
            [ReportFormat.TABLE],
            ["CVE-2023-48795"],
            None,
            id="list-table-generate_reports",
        ),
        pytest.param(
            ["Flattening purl-vulns dict", "Generate TABLE Report"],
            [ReportFormat.TABLE],
            {"purl": ["CVE-2023-48795"]},
            None,
            id="dict-table-generate_reports",
        ),
        pytest.param(
            ["Generate HTML Report"],
            [ReportFormat.HTML],
            ["CVE-2023-48795"],
            None,
            id="list-html-generate_reports",
        ),
        pytest.param(
            ["Flattening purl-vulns dict", "Generate HTML Report"],
            [ReportFormat.HTML],
            {"purl": ["CVE-2023-48795"]},
            None,
            id="dict-html-generate_reports",
        ),
        pytest.param(
            ["Generate Gitlab Report"],
            [ReportFormat.GITLAB],
            ["CVE-2023-48795"],
            None,
            id="list-gitlab-generate_reports",
        ),
        pytest.param(
            ["Flattening purl-vulns dict", "Generate Gitlab Report"],
            [ReportFormat.GITLAB],
            {"purl": ["CVE-2023-48795"]},
            None,
            id="dict-gitlab-generate_reports",
        ),
        pytest.param(
            ["Generate linked Report"],
            [ReportFormat.LINKED_VEX],
            ["CVE-2023-48795"],
            Sbom(),
            id="list-linked-generate_reports",
        ),
        pytest.param(
            ["Flattening purl-vulns dict", "Generate linked Report"],
            [ReportFormat.LINKED_VEX],
            {"purl": ["CVE-2023-48795"]},
            Sbom(),
            id="dict-linked-generate_reports",
        ),
        pytest.param(
            ["Generate embedded Report"],
            [ReportFormat.CYCLONE_DX],
            ["CVE-2023-48795"],
            Sbom(),
            id="list-cylonedx-generate_reports",
        ),
        pytest.param(
            ["Flattening purl-vulns dict", "Generate embedded Report"],
            [ReportFormat.CYCLONE_DX],
            {"purl": ["CVE-2023-48795"]},
            Sbom(),
            id="dict-cylonedx-generate_reports",
        ),
        pytest.param(
            [
                "Generate embedded Report",
                "Generate linked Report",
                "Generate Gitlab Report",
                "Generate HTML Report",
                "Generate TABLE Report",
            ],
            [
                ReportFormat.CYCLONE_DX,
                ReportFormat.LINKED_VEX,
                ReportFormat.GITLAB,
                ReportFormat.HTML,
                ReportFormat.TABLE,
            ],
            ["CVE-2023-48795"],
            Sbom(),
            id="list-all-generate_reports",
        ),
        pytest.param(
            [
                "Flattening purl-vulns dict",
                "Generate embedded Report",
                "Generate linked Report",
                "Generate Gitlab Report",
                "Generate HTML Report",
                "Generate TABLE Report",
            ],
            [
                ReportFormat.CYCLONE_DX,
                ReportFormat.LINKED_VEX,
                ReportFormat.GITLAB,
                ReportFormat.HTML,
                ReportFormat.TABLE,
            ],
            {"purl": ["CVE-2023-48795"]},
            Sbom(),
            id="dict-all-generate_reports",
        ),
        pytest.param(
            [],
            [],
            ["CVE-2023-48795"],
            None,
            id="list-none-generate_reports",
        ),
        pytest.param(
            ["Flattening purl-vulns dict"],
            [],
            {"purl": ["CVE-2023-48795"]},
            None,
            id="dict-none-generate_reports",
        ),
    ],
)
def test_generate_vulnerability_reports(
    expected_strings: list[str],
    formats: list[ReportFormat],
    vulnerabilities: list[Vulnerability] | Mapping[str, list[Vulnerability]],
    bom: Sbom | None,
    reporting: Reporting,
    capsys: pytest.CaptureFixture[str],
    monkeypatch: MonkeyPatch,
):
    all_strings = {
        "flatten": "Flattening purl-vulns dict",
        "embedded": "Generate embedded Report",
        "linked": "Generate linked Report",
        "gitlab": "Generate Gitlab Report",
        "html": "Generate HTML Report",
        "table": "Generate TABLE Report",
    }

    def _add_purl_as_bom_ref_and_flatten_patch(
        _, vulnerabilities: Mapping[str, list[Vulnerability]]
    ) -> list[Vulnerability]:
        rich.print(all_strings["flatten"])
        return [vuln for vulns in vulnerabilities.values() for vuln in vulns]

    with monkeypatch.context() as patch:
        patch.setattr(
            target=Reporting,
            name="_get_score",
            value=lambda _, __: 1,
        )
        patch.setattr(
            target=Reporting,
            name="_add_purl_as_bom_ref_and_flatten",
            value=_add_purl_as_bom_ref_and_flatten_patch,
        )
        patch.setattr(
            target=Reporting,
            name="_generate_cyclonedx_report",
            value=lambda *_, **__: rich.print(all_strings["embedded"]),
        )
        patch.setattr(
            target=Reporting,
            name="_generate_linked_report",
            value=lambda *_, **__: rich.print(all_strings["linked"]),
        )
        patch.setattr(
            target=Reporting,
            name="_generate_gitlab_vulnerability_report",
            value=lambda *_: rich.print(all_strings["gitlab"]),
        )
        patch.setattr(
            target=Reporting,
            name="_generate_html_report",
            value=lambda *_: rich.print(all_strings["html"]),
        )
        patch.setattr(
            target=Reporting,
            name="_generate_table_report",
            value=lambda *_: rich.print(all_strings["table"]),
        )

        reporting.generate_vulnerability_reports(formats, vulnerabilities, bom)

        out, error = capsys.readouterr()
        output_lines = list(filter(None, out.split("\n")))

        for expected_string in expected_strings:
            assert expected_string in output_lines

        for not_expected_string in [string for string in all_strings.values() if string not in expected_strings]:
            assert not_expected_string not in output_lines


@pytest.mark.parametrize(
    argnames=["expected", "purl_vuln_dict"],
    argvalues=[
        pytest.param(
            [
                Vulnerability(
                    id="CVE-2023-48795",
                    description="This is a test",
                    recommendation="test",
                    ratings=[
                        cdx.Rating(
                            score=5.9,
                            severity=cdx.Severity("medium"),
                            method=cdx.ScoreMethod("CVSSv31"),
                            vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                        )
                    ],
                    advisories=None,
                    affects=[Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                    tools=Tools(
                        components=[
                            Component(
                                type=ComponentType.APPLICATION,
                                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                                name="HopprCop",
                                version="1.0.0",
                                scope=cdx.Scope.EXCLUDED,
                            )
                        ]
                    ),
                )
            ],
            [
                {
                    "purl": "pkg:golang/golang.org/x/crypto@v0.14.0",
                    "vulns": [
                        {
                            "id": "CVE-2023-48795",
                            "ratings": [
                                cdx.Rating(
                                    score=5.9,
                                    severity=cdx.Severity("medium"),
                                    method=cdx.ScoreMethod("CVSSv31"),
                                    vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                                )
                            ],
                            "affects": [Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                        }
                    ],
                }
            ],
            id="basic-purl_as_bom_ref",
        ),
        pytest.param(
            [
                Vulnerability(
                    id="CVE-2023-48795",
                    description="This is a test",
                    recommendation="test",
                    ratings=[
                        cdx.Rating(
                            score=5.9,
                            severity=cdx.Severity("medium"),
                            method=cdx.ScoreMethod("CVSSv31"),
                            vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                        )
                    ],
                    advisories=None,
                    affects=[Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                    tools=Tools(
                        components=[
                            Component(
                                type=ComponentType.APPLICATION,
                                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                                name="HopprCop",
                                version="1.0.0",
                                scope=cdx.Scope.EXCLUDED,
                            )
                        ]
                    ),
                )
            ],
            [
                {
                    "purl": "pkg:golang/golang.org/x/crypto@v0.14.0",
                    "vulns": [
                        {
                            "id": "CVE-2023-48795",
                            "ratings": [
                                cdx.Rating(
                                    score=5.9,
                                    severity=cdx.Severity("medium"),
                                    method=cdx.ScoreMethod("CVSSv31"),
                                    vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                                )
                            ],
                            "affects": [],
                        }
                    ],
                }
            ],
            id="no-affects-purl_as_bom_ref",
        ),
        pytest.param(
            [
                Vulnerability(
                    id="CVE-2023-50000",
                    description="This is a test",
                    recommendation="test",
                    ratings=[
                        cdx.Rating(
                            score=7.5,
                            severity=cdx.Severity("high"),
                            method=cdx.ScoreMethod("CVSSv31"),
                            vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
                        )
                    ],
                    advisories=None,
                    affects=[Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                    tools=Tools(
                        components=[
                            Component(
                                type=ComponentType.APPLICATION,
                                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                                name="HopprCop",
                                version="1.0.0",
                                scope=cdx.Scope.EXCLUDED,
                            )
                        ]
                    ),
                ),
                Vulnerability(
                    id="CVE-2023-48795",
                    description="This is a test",
                    recommendation="test",
                    ratings=[
                        cdx.Rating(
                            score=5.9,
                            severity=cdx.Severity("medium"),
                            method=cdx.ScoreMethod("CVSSv31"),
                            vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                        )
                    ],
                    advisories=None,
                    affects=[Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                    tools=Tools(
                        components=[
                            Component(
                                type=ComponentType.APPLICATION,
                                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                                name="HopprCop",
                                version="1.0.0",
                                scope=cdx.Scope.EXCLUDED,
                            )
                        ]
                    ),
                ),
            ],
            [
                {
                    "purl": "pkg:golang/golang.org/x/crypto@v0.14.0",
                    "vulns": [
                        {
                            "id": "CVE-2023-48795",
                            "ratings": [
                                cdx.Rating(
                                    score=5.9,
                                    severity=cdx.Severity("medium"),
                                    method=cdx.ScoreMethod("CVSSv31"),
                                    vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                                )
                            ],
                            "affects": [Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                        },
                        {
                            "id": "CVE-2023-50000",
                            "ratings": [
                                cdx.Rating(
                                    score=7.5,
                                    severity=cdx.Severity("high"),
                                    method=cdx.ScoreMethod("CVSSv31"),
                                    vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
                                )
                            ],
                            "affects": [Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                        },
                    ],
                },
            ],
            id="multiple-vuln_as_bom_ref",
        ),
        pytest.param(
            [
                Vulnerability(
                    id="CVE-2023-48795",
                    description="This is a test",
                    recommendation="test",
                    ratings=[
                        cdx.Rating(
                            score=5.9,
                            severity=cdx.Severity("medium"),
                            method=cdx.ScoreMethod("CVSSv31"),
                            vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                        )
                    ],
                    advisories=None,
                    affects=[Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                    tools=Tools(
                        components=[
                            Component(
                                type=ComponentType.APPLICATION,
                                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                                name="HopprCop",
                                version="1.0.0",
                                scope=cdx.Scope.EXCLUDED,
                            )
                        ]
                    ),
                ),
                Vulnerability(
                    id="CVE-2023-48795",
                    description="This is a test",
                    recommendation="test",
                    ratings=[
                        cdx.Rating(
                            score=5.9,
                            severity=cdx.Severity("medium"),
                            method=cdx.ScoreMethod("CVSSv31"),
                            vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                        )
                    ],
                    advisories=None,
                    affects=[Affect(ref="pkg:golang/golang.org/another@v0.1.0")],
                    tools=Tools(
                        components=[
                            Component(
                                type=ComponentType.APPLICATION,
                                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                                name="HopprCop",
                                version="1.0.0",
                                scope=cdx.Scope.EXCLUDED,
                            )
                        ]
                    ),
                ),
            ],
            [
                {
                    "purl": "pkg:golang/golang.org/x/crypto@v0.14.0",
                    "vulns": [
                        {
                            "id": "CVE-2023-48795",
                            "ratings": [
                                cdx.Rating(
                                    score=5.9,
                                    severity=cdx.Severity("medium"),
                                    method=cdx.ScoreMethod("CVSSv31"),
                                    vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                                )
                            ],
                            "affects": [Affect(ref="pkg:golang/golang.org/x/crypto@v0.14.0")],
                        },
                    ],
                },
                {
                    "purl": "pkg:golang/golang.org/another@v0.1.0",
                    "vulns": [
                        {
                            "id": "CVE-2023-48795",
                            "ratings": [
                                cdx.Rating(
                                    score=5.9,
                                    severity=cdx.Severity("medium"),
                                    method=cdx.ScoreMethod("CVSSv31"),
                                    vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                                )
                            ],
                            "affects": [Affect(ref="pkg:golang/golang.org/another@v0.1.0")],
                        },
                    ],
                },
            ],
            id="multiple-purl_as_bom_ref",
        ),
    ],
    indirect=["purl_vuln_dict"],
)
def test__add_purl_as_bom_ref_and_flatten(
    expected: list[Vulnerability],
    purl_vuln_dict: dict[str, list[Vulnerability]],
    reporting: Reporting,
    monkeypatch: MonkeyPatch,
):
    def get_score_patch(_, vuln: Vulnerability) -> float:
        if not vuln.ratings or vuln.ratings[0].score is None:
            return 0.0
        return vuln.ratings[0].score

    with monkeypatch.context() as patch:
        patch.setattr(target=Reporting, name="_get_score", value=get_score_patch)

        actual = reporting._add_purl_as_bom_ref_and_flatten(purl_vuln_dict)

        assert actual == expected


@pytest.mark.parametrize(
    argnames=["sbom"],
    argvalues=[
        pytest.param(Path("bom/unit_syft_java_bom.json"), id="basic-hoppr-cop-tool"),
        pytest.param("empty", id="empty-hoppr-cop-tool"),
    ],
    indirect=["sbom"],
)
def test__set_hopprcop_as_tool(
    sbom: Sbom,
    reporting: Reporting,
    monkeypatch: MonkeyPatch,
):
    expected = Component(
        type=cdx.Type("application"),
        name="hoppr-cop",
        version=hopprcop.__version__,
        bom_ref=f"pkg:pypi/hoppr-cop@{hopprcop.__version__}",
        purl=f"pkg:pypi/hoppr-cop@{hopprcop.__version__}",
        scope=cdx.Scope.EXCLUDED,
    )

    reporting._set_hopprcop_as_tool(sbom)

    assert sbom.metadata
    assert sbom.metadata.tools
    assert sbom.metadata.tools.components
    assert expected in sbom.metadata.tools.components


@pytest.mark.parametrize(
    argnames=["vulnerabilities"],
    argvalues=[
        pytest.param(
            ["CVE-2024-12345"],
            id="basic-cyclonedx-report",
        ),
        pytest.param(
            ["CVE-2024-12345", "CVE-2023-12345", "GHSA-1980-12345", "TEMP-12903123"],
            id="multi-cyclonedx-report",
        ),
        pytest.param(
            [],
            id="none-cyclonedx-report",
        ),
    ],
    indirect=["vulnerabilities"],
)
def test__generate_cyclonedx_report(
    vulnerabilities: list[Vulnerability],
    sbom: Sbom,
    reporting: Reporting,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    serial_number = uuid.uuid4()

    with monkeypatch.context() as patch:
        patch.setattr(target=uuid, name="uuid4", value=lambda *_: serial_number)
        patch.setattr(
            target=Reporting,
            name="_set_hopprcop_as_tool",
            value=lambda *_: True,
        )

        reporting._generate_cyclonedx_report(sbom, vulnerabilities)

        actual_file = tmp_path / f"{Path(reporting.base_name).name}-enhanced.json"

        assert actual_file.exists()
        actual = Sbom.parse_file(actual_file)
        assert actual.serialNumber == serial_number.urn
        assert actual.vulnerabilities == vulnerabilities


@pytest.mark.parametrize(
    argnames=["linked_vulnerabilities", "vulnerabilities"],
    argvalues=[
        pytest.param(
            ["CVE-2024-12345"],
            ["CVE-2024-12345"],
            id="basic-linked-report",
        ),
        pytest.param(
            ["CVE-2024-12345", "CVE-2023-12345", "GHSA-1980-12345", "TEMP-12903123"],
            ["CVE-2024-12345", "CVE-2023-12345", "GHSA-1980-12345", "TEMP-12903123"],
            id="multi-linked-report",
        ),
        pytest.param(
            [],
            [],
            id="none-linked-report",
        ),
    ],
    indirect=["linked_vulnerabilities", "vulnerabilities"],
)
def test__generate_linked_report(
    linked_vulnerabilities: list[Vulnerability],
    vulnerabilities: list[Vulnerability],
    sbom: Sbom,
    reporting: Reporting,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    serial_number = uuid.uuid4()

    with monkeypatch.context() as patch:
        patch.setattr(target=uuid, name="uuid4", value=lambda *_: serial_number)
        patch.setattr(
            target=Reporting,
            name="_set_hopprcop_as_tool",
            value=lambda *_: True,
        )

        reporting._generate_linked_report(sbom, vulnerabilities)

        actual_file = tmp_path / f"{Path(reporting.base_name).name}-vex.json"

        assert actual_file.exists()
        actual = Sbom.parse_file(actual_file)
        assert actual.serialNumber == serial_number.urn
        assert actual.vulnerabilities == linked_vulnerabilities


@pytest.mark.parametrize(
    argnames=["expected", "vulnerability", "ratings"],
    argvalues=[
        pytest.param(
            5.9,
            {"id": "CVE-2023-48795"},
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            id="basic-get_score",
        ),
        pytest.param(
            0.0,
            {"id": "CVE-2023-48795"},
            [],
            id="no-rating-get_score",
        ),
    ],
    indirect=["vulnerability", "ratings"],
)
def test_get_score(
    expected: float,
    vulnerability: Vulnerability,
    ratings: list[cdx.Rating],
    reporting: Reporting,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(
            target=Reporting, name="_get_best_rating", value=lambda _, ratings: ratings[0] if ratings else None
        )

        actual = reporting._get_score(vulnerability)

        assert actual == expected


def test__generate_table_report(
    reporting: Reporting,
    capsys: pytest.CaptureFixture[str],
    monkeypatch: MonkeyPatch,
):
    expected_strings = [
        "  type   name   version   id         severity   found by  ",
        "  test   fake   v0.0.0    CVE-none   medium     HopprCop  ",
    ]
    with monkeypatch.context() as patch:
        patch.setattr(
            target=Reporting,
            name="_get_fields_from_vulnerabilities",
            value=lambda *_: [["test", "fake", "v0.0.0", "CVE-none", "medium", "HopprCop"]],
        )

        reporting._generate_table_report([])

        out, error = capsys.readouterr()
        output_lines = list(filter(None, out.split("\n")))

        for expected_string in expected_strings:
            assert expected_string in output_lines


@pytest.mark.parametrize(
    argnames=["expected", "vulnerabilities", "ratings", "affects", "severity"],
    argvalues=[
        pytest.param(
            ["golang", "crypto", "v0.14.0", "CVE-2023-48795", "medium", "Hoppr HopprCop"],
            ["CVE-2023-48795"],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}],
            "medium",
            id="basic-get_fields",
        ),
        pytest.param(
            ["golang", "crypto", "v0.14.0", "CVE-2023-48795", "medium", "Hoppr HopprCop"],
            ["CVE-2023-48795"],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            [
                {"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"},
                {"ref": "pkg:golang/golang.org/x/crypto@v0.14.0?qualifier=test"},
            ],
            "medium",
            id="duplicate-get_fields",
        ),
        pytest.param(
            ["golang", "grpc", "v1.57.0", "GHSA-m425-mq94-257g", "[bright_yellow]high[/]", "Hoppr HopprCop"],
            ["GHSA-m425-mq94-257g"],
            [
                {
                    "score": "7.5",
                    "severity": "high",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
                }
            ],
            [{"ref": "pkg:golang/google.golang.org/grpc@v1.57.0"}],
            "high",
            id="high-get_fields",
        ),
        pytest.param(
            ["maven", "spring-beans", "5.3.16", "CVE-2022-22965", "[red]critical[/]", "Hoppr HopprCop"],
            ["CVE-2022-22965"],
            [
                {
                    "score": "9.8",
                    "severity": "critical",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                }
            ],
            [{"ref": "pkg:maven/org.springframework/spring-beans@5.3.16", "versions": [{"version": "5.3.16"}]}],
            "critical",
            id="critical-get_fields",
        ),
        pytest.param(
            [],
            ["CVE-2023-48795"],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            [],
            "medium",
            id="no-affects-get_fields",
        ),
    ],
    indirect=["vulnerabilities", "ratings", "affects"],
)
def test__get_fields_from_vulnerabilities(
    expected: list[str],
    vulnerabilities: list[Vulnerability],
    severity: str,
    reporting: Reporting,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=Reporting, name="_get_severity", value=lambda *_: severity)

        actual = reporting._get_fields_from_vulnerabilities(vulnerabilities)

        assert len(actual[0]) == len(expected)
        assert actual[0] == expected


@pytest.mark.parametrize(
    argnames=["expected"],
    argvalues=[
        pytest.param(
            "vulnerabilities.css",
            id="basic-copy_assets",
        ),
    ],
)
def test__copy_assets(
    expected: str,
    reporting: Reporting,
    tmp_path: Path,
):
    reporting._copy_assets()

    actual_path = tmp_path / "assets"

    assert (actual_path / expected).exists()


@pytest.mark.parametrize(
    argnames=["expected_strings", "vulnerability", "severity"],
    argvalues=[
        pytest.param(
            [
                '<td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">\n                            <a href="unit_test-details/CVE-2023-48795.html"\n                               class="text-blue-600 hover:text-blue-700 transition duration-300 ease-in-out mb-4">CVE-2023-48795</a></td>',
                '<div class="bg-gray-50 rounded-lg py-5 px-6 mb-4 text-base text-gray-500 mb-3" role="alert">\n                                medium\n                            </div>',
            ],
            {"id": "CVE-2023-48795"},
            "medium",
            id="basic-generate_html",
        ),
    ],
    indirect=["vulnerability"],
)
def test__generate_html_report(
    expected_strings: list[str],
    vulnerability: Vulnerability,
    severity: str,
    reporting: Reporting,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(
            target=Reporting,
            name="_get_severity",
            value=lambda *_: severity,
        )
        patch.setattr(
            target=Reporting,
            name="_copy_assets",
            value=lambda *_: True,
        )
        patch.setattr(
            target=Reporting,
            name="_generate_vuln_detail_reports",
            value=lambda *_: True,
        )

        reporting._generate_html_report([vulnerability] if vulnerability else [])

        actual_file = tmp_path / f"{Path(reporting.base_name).name}-vulnerabilities.html"

        assert actual_file.exists()
        actual = actual_file.read_text()
        for expected_string in expected_strings:
            assert expected_string in actual


@pytest.mark.parametrize(
    argnames=["expected", "expected_strings", "vulnerability", "affects"],
    argvalues=[
        pytest.param(
            "CVE-2023-48795.html",
            [
                '<a href="" target="_blank"\n       class="text-blue-600 hover:text-blue-700 transition duration-300 ease-in-out mb-4">CVE-2023-48795</a>',
                '<td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">golang</td>',
                '<td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">golang.org/x</td>',
                '<td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">crypto</td>',
                '<td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">v0.14.0</td>',
                '<td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">pkg:golang/golang.org/x/crypto@v0.14.0</td>',
            ],
            {"id": "CVE-2023-48795"},
            [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}],
            id="basic-vuln_detail",
        ),
        pytest.param(
            "",
            [],
            {"id": "CVE-2023-48795"},
            [],
            id="no-affects-vuln_detail",
        ),
        pytest.param(
            "",
            [],
            None,
            [],
            id="none-vuln_detail",
        ),
    ],
    indirect=["vulnerability", "affects"],
)
def test__generate_vuln_detail_reports(
    expected: str,
    expected_strings: list[str],
    vulnerability: Vulnerability,
    affects: list[Affect],
    severity_classes: dict[str, str],
    reporting: Reporting,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=Reporting, name="_get_featured_link", value=lambda *_: None)

        reporting._generate_vuln_detail_reports([vulnerability] if vulnerability else [], severity_classes)

        actual_file = tmp_path / f"{Path(reporting.base_name).name}-details" / expected

        assert actual_file.exists()
        actual = actual_file.read_text() if actual_file.is_file() else ""
        for expected_string in expected_strings:
            assert expected_string in actual


@pytest.mark.parametrize(
    argnames=["expected_scan", "expected_vuln", "expected_dep", "vulnerability", "gitlab_row"],
    argvalues=[
        pytest.param(
            '"analyzer": {\n      "id": "hoppr-cop",\n      "name": "Hoppr Cop",\n      "vendor": {\n        "name": "Hoppr"\n      },\n      "version": "VERSION"\n    },\n    "scanner": {\n      "id": "hoppr-cop",\n      "name": "Hoppr Cop",\n      "version": "VERSION",\n      "vendor": {\n        "name": "Hoppr"\n      }\n    },',
            '"vulnerabilities": [\n    {\n      "id": "CVE-2023-48795",\n      "name": "CVE-2023-48795",',
            '"dependency_files": [\n    {\n      "path": "cyclonedx.bom",\n      "package_manager": "golang",\n      "dependencies": [\n        {\n          "package": {\n            "name": "crypto"\n          },\n          "version": "v0.14.0"\n        }\n      ]\n    }\n  ]',
            {"id": "CVE-2023-48795"},
            [
                GitlabVulnerability(
                    id="CVE-2023-48795",
                    name="CVE-2023-48795",
                    description="this is a test",
                    severity="Medium",
                    solution="State: fixed | Fix Versions: 0.17.0",
                    identifiers=[Identifier(type="cve", name="CVE-2023-48795", value="CVE-2023-48795")],
                    cvss_vectors=[
                        CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")
                    ],
                    links=[Link(url="https://fake-url")],
                    location=Location(
                        file="cyclonedx.bom",
                        dependency=Dependency(
                            package=Package(name="crypto"),
                            version="v0.14.0",
                        ),
                    ),
                )
            ],
            id="basic-generate_gitlab_vulnerability_report",
        ),
        pytest.param(
            '"analyzer": {\n      "id": "hoppr-cop",\n      "name": "Hoppr Cop",\n      "vendor": {\n        "name": "Hoppr"\n      },\n      "version": "VERSION"\n    },\n    "scanner": {\n      "id": "hoppr-cop",\n      "name": "Hoppr Cop",\n      "version": "VERSION",\n      "vendor": {\n        "name": "Hoppr"\n      }\n    },',
            '"vulnerabilities": []',
            '"dependency_files": []',
            None,
            None,
            id="empty-generate_gitlab_vulnerability_report",
        ),
    ],
    indirect=["vulnerability"],
)
def test_generate_gitlab_vulnerability_report(
    expected_scan: str,
    expected_vuln: str,
    expected_dep: str,
    vulnerability: Vulnerability | None,
    gitlab_row: list[GitlabVulnerability],
    reporting: Reporting,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=Reporting, name="_generate_gitlab_row", value=lambda *_: gitlab_row)

        reporting._generate_gitlab_vulnerability_report([vulnerability] if vulnerability else [])

        output_file = tmp_path / "gl-dependency-scanning-report.json"

        actual = output_file.read_text()

        assert output_file.exists()
        assert expected_scan.replace("VERSION", hopprcop.__version__) in actual
        assert expected_vuln in actual
        assert expected_dep in actual


@pytest.mark.parametrize(
    argnames=["expected", "advisories"],
    argvalues=[
        pytest.param(
            None,
            [cdx.Advisory(url="https://fake-url")],
            id="basic-get_featured_link",
        ),
        pytest.param(
            "https://snyk.io/",
            [cdx.Advisory(url="https://snyk.io/")],
            id="snyk-get_featured_link",
        ),
        pytest.param(
            "https://snyk.io/extra/path",
            [
                cdx.Advisory(url="https://fake-url"),
                cdx.Advisory(url="https://snyk.io/extra/path"),
                cdx.Advisory(url="https://fake-url2"),
            ],
            id="multiple-get_featured_link",
        ),
        pytest.param(
            None,
            None,
            id="none-get_featured_link",
        ),
    ],
)
def test__get_featured_link(expected: str | None, advisories: list[cdx.Advisory] | None, reporting: Reporting):
    actual = reporting._get_featured_link(advisories)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "rating"],
    argvalues=[
        pytest.param(
            "medium",
            cdx.Rating(
                score=5.9,
                severity=cdx.Severity("medium"),
                method=cdx.ScoreMethod("CVSSv31"),
                vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
            ),
            id="basic-get_severity",
        ),
        pytest.param(
            "none",
            None,
            id="none-get_severity",
        ),
    ],
)
def test__get_severity(expected: str, rating: cdx.Rating, reporting: Reporting, monkeypatch: MonkeyPatch):
    with monkeypatch.context() as patch:
        patch.setattr(
            target=Reporting,
            name="_get_best_rating",
            value=lambda _, rating_list: rating_list[0] if rating_list else None,
        )

        actual = reporting._get_severity([rating])

        assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "ratings"],
    argvalues=[
        pytest.param(
            cdx.Rating(
                score=5.9,
                severity=cdx.Severity("medium"),
                method=cdx.ScoreMethod("CVSSv31"),
                vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
            ),
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            id="basic-best_rating",
        ),
        pytest.param(
            cdx.Rating(
                score=5.9,
                severity=cdx.Severity("medium"),
                method=cdx.ScoreMethod("CVSSv31"),
                vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
            ),
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                },
                {
                    "score": "9.8",
                    "severity": "medium",
                    "method": "CVSSv3",
                    "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                },
                {
                    "score": "6.8",
                    "severity": "medium",
                    "method": "CVSSv2",
                    "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:P",
                },
            ],
            id="multi-best_rating",
        ),
        pytest.param(
            cdx.Rating(
                score=5.9,
                severity=cdx.Severity("medium"),
                method=cdx.ScoreMethod("CVSSv31"),
                vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
            ),
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                },
                {
                    "score": "9.8",
                    "severity": "critical",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                },
                {
                    "source": {"name": "ghsa"},
                    "score": "9.8",
                    "severity": "critical",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                },
            ],
            id="same-best_rating",
        ),
        pytest.param(
            cdx.Rating(
                score=9.8,
                severity=cdx.Severity("medium"),
                method=cdx.ScoreMethod("CVSSv3"),
                vector="CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            ),
            [
                {
                    "score": "9.8",
                    "severity": "medium",
                    "method": "CVSSv3",
                    "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                }
            ],
            id="cvssv3-best_rating",
        ),
        pytest.param(
            cdx.Rating(
                score=6.8,
                severity=cdx.Severity("medium"),
                method=cdx.ScoreMethod("CVSSv2"),
                vector="AV:N/AC:M/Au:N/C:P/I:P/A:P",
            ),
            [
                {
                    "score": "6.8",
                    "severity": "medium",
                    "method": "CVSSv2",
                    "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:P",
                }
            ],
            id="cvssv2-best_rating",
        ),
        pytest.param(
            None,
            [],
            id="empty-best_rating",
        ),
        pytest.param(
            None,
            None,
            id="none-best_rating",
        ),
    ],
    indirect=["ratings"],
)
def test__get_best_rating(
    expected: cdx.Rating | None,
    ratings: list[cdx.Rating] | None,
    reporting: Reporting,
):
    actual = reporting._get_best_rating(ratings)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "ratings"],
    argvalues=[
        pytest.param(
            [CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                }
            ],
            id="basic-cvss",
        ),
        pytest.param(
            [
                CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N"),
                CvssVectors3x(vendor="redhat", vector="CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"),
            ],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                },
                {
                    "source": {"name": "redhat"},
                    "score": "9.8",
                    "severity": "medium",
                    "method": "CVSSv3",
                    "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                },
            ],
            id="multiple-cvss",
        ),
        pytest.param(
            [
                CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N"),
                CvssVectors(vendor="redhat", vector="AV:N/AC:M/Au:N/C:P/I:P/A:P"),
            ],
            [
                {
                    "score": "5.9",
                    "severity": "medium",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
                },
                {
                    "source": {"name": "redhat"},
                    "score": "6.8",
                    "severity": "medium",
                    "method": "CVSSv2",
                    "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:P",
                },
            ],
            id="mixed-cvss",
        ),
        pytest.param(
            [
                CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"),
            ],
            [
                {
                    "score": "9.8",
                    "severity": "critical",
                    "method": "CVSSv31",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
                },
                {
                    "severity": "medium",
                    "method": "other",
                },
            ],
            id="not-cvss",
        ),
    ],
    indirect=["ratings"],
)
def test__get_cvss_vectors(
    expected: list[CvssVectors3x | CvssVectors],
    ratings: list[cdx.Rating],
    reporting: Reporting,
):
    actual = reporting._get_cvss_vectors(ratings)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "vulnerability", "affects", "advisories", "severity", "vectors"],
    argvalues=[
        pytest.param(
            [
                GitlabVulnerability(
                    id="uuid-1234",
                    name="CVE-2023-48795",
                    description="this is a test",
                    severity="Medium",
                    solution="test recomendation",
                    identifiers=[Identifier(type="cve", name="CVE-2023-48795", value="CVE-2023-48795")],
                    cvss_vectors=[
                        CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")
                    ],
                    links=[Link(url="https://fake-url")],
                    location=Location(
                        file="cyclonedx.bom",
                        dependency=Dependency(
                            package=Package(name="crypto"),
                            version="v0.14.0",
                        ),
                    ),
                    details={
                        "vulnerable_package": NamedFieldTextItem(name="Vulnerable Package", value="crypto:v0.14.0"),
                        "found_by": NamedFieldTextItem(
                            type="text", value="HopprCop", name="Found by", description=None
                        ),
                    },
                )
            ],
            {"id": "CVE-2023-48795"},
            [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}],
            [{"url": "https://fake-url"}],
            "medium",
            [CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")],
            id="basic-gitlab-row",
        ),
        pytest.param(
            [
                GitlabVulnerability(
                    id="uuid-1234",
                    name="CVE-2023-48795",
                    description="this is a test",
                    severity="Info",
                    solution="test recomendation",
                    identifiers=[Identifier(type="cve", name="CVE-2023-48795", value="CVE-2023-48795")],
                    cvss_vectors=None,
                    links=[],
                    location=Location(
                        file="cyclonedx.bom",
                        dependency=Dependency(
                            package=Package(name="crypto"),
                            version="v0.14.0",
                        ),
                    ),
                    details={
                        "vulnerable_package": NamedFieldTextItem(name="Vulnerable Package", value="crypto:v0.14.0"),
                        "found_by": NamedFieldTextItem(
                            type="text", value="HopprCop", name="Found by", description=None
                        ),
                    },
                )
            ],
            {"id": "CVE-2023-48795"},
            [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}],
            [],
            "none",
            [],
            id="defaults-gitlab-row",
        ),
        pytest.param(
            [
                GitlabVulnerability(
                    id="uuid-1234",
                    name="CVE-2023-48795",
                    description="this is a test",
                    severity="Medium",
                    solution="test recomendation",
                    identifiers=[Identifier(type="cve", name="CVE-2023-48795", value="CVE-2023-48795")],
                    cvss_vectors=[
                        CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")
                    ],
                    links=[Link(url="https://fake-url")],
                    location=Location(
                        file="cyclonedx.bom",
                        dependency=Dependency(
                            package=Package(name="crypto"),
                            version="v0.14.0",
                        ),
                    ),
                    details={
                        "vulnerable_package": NamedFieldTextItem(name="Vulnerable Package", value="crypto:v0.14.0"),
                        "found_by": NamedFieldTextItem(
                            type="text", value="HopprCop", name="Found by", description=None
                        ),
                    },
                ),
                GitlabVulnerability(
                    id="uuid-1234",
                    name="CVE-2023-48795",
                    description="this is a test",
                    severity="Medium",
                    solution="test recomendation",
                    identifiers=[Identifier(type="cve", name="CVE-2023-48795", value="CVE-2023-48795")],
                    cvss_vectors=[
                        CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")
                    ],
                    links=[Link(url="https://fake-url")],
                    location=Location(
                        file="cyclonedx.bom",
                        dependency=Dependency(
                            package=Package(name="qs"),
                            version="6.5.2",
                        ),
                    ),
                    details={
                        "vulnerable_package": NamedFieldTextItem(name="Vulnerable Package", value="qs:6.5.2"),
                        "found_by": NamedFieldTextItem(
                            type="text", value="HopprCop", name="Found by", description=None
                        ),
                    },
                ),
            ],
            {"id": "CVE-2023-48795"},
            [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}, {"ref": "pkg:npm/qs@6.5.2"}],
            [{"url": "https://fake-url"}],
            "medium",
            [CvssVectors3x(vendor="unknown", vector="CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N")],
            id="multi-affects-gitlab-row",
        ),
        pytest.param(
            [],
            {"id": None},
            [],
            [],
            "",
            [],
            id="missing-vuln-gitlab-row",
        ),
    ],
    indirect=["vulnerability", "affects", "advisories"],
)
def test__generate_gitlab_row(
    expected: list[GitlabVulnerability],
    vulnerability: Vulnerability,
    severity: str,
    vectors: list[CvssVectors3x | CvssVectors] | None,
    reporting: Reporting,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=uuid, name="uuid4", value=lambda *_: "uuid-1234")
        patch.setattr(target=Reporting, name="_get_cvss_vectors", value=lambda *_: vectors)
        patch.setattr(target=Reporting, name="_get_severity", value=lambda *_: severity)

        actual = reporting._generate_gitlab_row(vulnerability)

        assert actual == expected
