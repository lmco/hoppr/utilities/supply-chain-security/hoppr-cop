from __future__ import annotations

from subprocess import Popen  # noqa: TC003
from typing import TYPE_CHECKING, TypeAlias

import pytest

from hoppr import Sbom, Vulnerability

import hopprcop.grype.grype_scanner

from hopprcop.grype.grype_scanner import GrypeScanner
from hopprcop.grype.models import (
    Artifact,
    GrypeResult,
    Match,
    Vulnerability as GrypeVulnerability,
)


if TYPE_CHECKING:
    from pathlib import Path


FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="generic_response")
def grype_response_fixture(request: FixtureRequest, resources_dir: Path) -> tuple[bytes, bytes]:
    relative_path = getattr(request, "param", "grype/unit_grype_java_result.json")
    grype_file = resources_dir / relative_path
    return (grype_file.read_bytes(), b"error string")


@pytest.fixture(name="grype_result")
def grype_result_fixture(grype_response: bytes) -> GrypeResult:
    return GrypeResult.parse_raw(grype_response)


@pytest.fixture(name="grype_match")
def grype_match_fixture(request: bytes) -> Match:
    grype_result = getattr(request, "param", {})
    vuln_dict = grype_result.get(
        "vulnerability",
        {
            "id": "GHSA-hr8g-6v94-x4m9",
            "data_source": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
            "namespace": "github:language:java",
            "urls": ["https://github.com/advisories/GHSA-hr8g-6v94-x4m9"],
            "description": "test",
            "severity": "Medium",
            "cvss": [
                {
                    "version": "3.1",
                    "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                    "metrics": {"base_score": 5.3, "exploitability_score": 3.9, "impact_score": 1.4},
                    "vendor_metadata": {"base_severity": "Medium", "status": "N/A"},
                }
            ],
            "fix": {"versions": [], "state": "not-fixed"},
            "advisories": [],
        },
    )
    related_vulns = grype_result.get(
        "related_vulnerabilities",
        [
            {
                "id": "CVE-2023-33201",
                "data_source": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201",
                "namespace": "nvd:cpe",
                "urls": [
                    "https://bouncycastle.org",
                    "https://github.com/bcgit/bc-java/commit/e8c409a8389c815ea3fda5e8b94c92fdfe583bcc",
                    "https://github.com/bcgit/bc-java/wiki/CVE-2023-33201",
                    "https://lists.debian.org/debian-lts-announce/2023/08/msg00000.html",
                    "https://security.netapp.com/advisory/ntap-20230824-0008/",
                ],
                "description": "test",
                "severity": "Medium",
                "cvss": [
                    {
                        "source": "nvd@nist.gov",
                        "type": "Primary",
                        "version": "3.1",
                        "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                        "metrics": {"base_score": 5.3, "exploitability_score": 3.9, "impact_score": 1.4},
                    }
                ],
            }
        ],
    )
    artifact = grype_result.get(
        "artifact",
        {
            "id": "8c3c2be831ddeb02",
            "name": "bcprov-jdk15on",
            "version": "1.68",
            "type": "java-archive",
            "locations": [],
            "language": "java",
            "licenses": ["Bouncy Castle Licence"],
            "cpes": [],
            "purl": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            "upstreams": [],
        },
    )

    return Match(
        vulnerability=GrypeVulnerability.parse_obj(vuln_dict),
        related_vulnerabilities=[GrypeVulnerability.parse_obj(related_vuln) for related_vuln in related_vulns],
        artifact=Artifact.parse_obj(artifact),
    )


@pytest.mark.parametrize(
    argnames=["expected_env", "offline", "os_distro"],
    argvalues=[
        pytest.param(
            [],
            False,
            None,
            id="default-init",
        ),
        pytest.param(
            ["GRYPE_DB_AUTO_UPDATE", "GRYPE_DB_VALIDATE_AGE"],
            True,
            None,
            id="offline-init",
        ),
        pytest.param(
            [],
            False,
            "Debian",
            id="os-init",
        ),
        pytest.param(
            ["GRYPE_DB_AUTO_UPDATE", "GRYPE_DB_VALIDATE_AGE"],
            True,
            "RHEL",
            id="os-offline-init",
        ),
    ],
)
def test_init(
    expected_env: list[str],
    offline: bool,
    os_distro: str | None,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setenv(name="OS_DISTRIBUTION", value="ENV_OS")
        patch.delenv(name="GRYPE_DB_AUTO_UPDATE", raising=False)
        patch.delenv(name="GRYPE_DB_VALIDATE_AGE", raising=False)

        actual = GrypeScanner(offline, os_distro)

        assert actual.grype_os_distro == (os_distro or "ENV_OS")
        assert actual.offline_mode == offline
        assert all(env in (actual.process_environment or {}) for env in expected_env)


@pytest.mark.parametrize(
    argnames=["expected", "generic_response"],
    argvalues=[
        pytest.param(
            True,
            "grype/grype_db_update_normal.txt",
            id="successful-db",
        ),
        pytest.param(
            False,
            "grype/grype_db_update_broken.txt",
            id="error-db",
        ),
    ],
    indirect=["generic_response"],
)
def test_get_vulnerability_db(
    expected: bool,
    generic_response: tuple[bytes, bytes],
    scanner: GrypeScanner,
    popen_patch: type[Popen],
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.grype.grype_scanner, name="Popen", value=popen_patch)

        actual = scanner.get_vulnerability_db()

        assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "grype_match", "purls_list"],
    argvalues=[
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "references": [
                        {
                            "id": "GHSA-hr8g-6v94-x4m9",
                            "source": {
                                "url": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                                "name": "Github Advisories",
                            },
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.3,
                            "severity": "medium",
                            "method": "CVSSv31",
                            "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                        }
                    ],
                    "description": "test",
                    "recommendation": "State: not-fixed | Fix Versions: ",
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                }
            ),
            {
                "vulnerability": {
                    "id": "GHSA-hr8g-6v94-x4m9",
                    "data_source": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                    "namespace": "github:language:java",
                    "urls": ["https://github.com/advisories/GHSA-hr8g-6v94-x4m9"],
                    "description": "test",
                    "severity": "Medium",
                    "cvss": [
                        {
                            "version": "3.1",
                            "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                            "metrics": {"base_score": 5.3, "exploitability_score": 3.9, "impact_score": 1.4},
                            "vendor_metadata": {"base_severity": "Medium", "status": "N/A"},
                        }
                    ],
                    "fix": {"versions": [], "state": "not-fixed"},
                },
                "related_vulnerabilities": [
                    {
                        "id": "CVE-2023-33201",
                        "data_source": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201",
                        "urls": [
                            "https://bouncycastle.org",
                        ],
                        "description": "test",
                        "severity": "Medium",
                        "cvss": [
                            {
                                "source": "nvd@nist.gov",
                                "type": "Primary",
                                "version": "3.1",
                                "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                                "metrics": {"base_score": 5.3, "exploitability_score": 3.9, "impact_score": 1.4},
                            }
                        ],
                    }
                ],
                "artifact": {
                    "id": "8c3c2be831ddeb02",
                    "version": "1.68",
                    "purl": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                },
            },
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="basic-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "references": [
                        {
                            "id": "GHSA-hr8g-6v94-x4m9",
                            "source": {
                                "url": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                                "name": "Github Advisories",
                            },
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.8,
                            "severity": "medium",
                            "method": "CVSSv2",
                            "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                        }
                    ],
                    "description": "test",
                    "recommendation": "State: not-fixed | Fix Versions: ",
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                }
            ),
            {
                "vulnerability": {
                    "id": "GHSA-hr8g-6v94-x4m9",
                    "data_source": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                    "namespace": "github:language:java",
                    "urls": ["https://github.com/advisories/GHSA-hr8g-6v94-x4m9"],
                    "description": "test",
                    "severity": "Medium",
                    "cvss": [
                        {
                            "version": "2.0",
                            "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                            "metrics": {"base_score": 5.8, "exploitability_score": 8.6, "impact_score": 4.9},
                            "vendor_metadata": {"base_severity": "Medium", "status": "N/A"},
                        }
                    ],
                    "fix": {"versions": [], "state": "not-fixed"},
                },
                "related_vulnerabilities": [
                    {
                        "id": "CVE-2023-33201",
                        "data_source": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201",
                        "urls": [
                            "https://bouncycastle.org",
                        ],
                        "description": "test",
                        "severity": "Medium",
                        "cvss": [
                            {
                                "source": "nvd@nist.gov",
                                "type": "Primary",
                                "version": "2.0",
                                "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                                "metrics": {"base_score": 5.8, "exploitability_score": 8.6, "impact_score": 4.9},
                            }
                        ],
                    }
                ],
                "artifact": {
                    "id": "8c3c2be831ddeb02",
                    "version": "1.68",
                    "purl": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                },
            },
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="cvss2-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "references": [
                        {
                            "id": "GHSA-hr8g-6v94-x4m9",
                            "source": {
                                "url": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                                "name": "Github Advisories",
                            },
                        }
                    ],
                    "ratings": [
                        {
                            "severity": "medium",
                            "method": "other",
                        }
                    ],
                    "description": "test",
                    "recommendation": "State: not-fixed | Fix Versions: ",
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                }
            ),
            {
                "vulnerability": {
                    "id": "GHSA-hr8g-6v94-x4m9",
                    "data_source": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                    "namespace": "github:language:java",
                    "urls": ["https://github.com/advisories/GHSA-hr8g-6v94-x4m9"],
                    "description": "test",
                    "severity": "Medium",
                    "cvss": [],
                    "fix": {"versions": [], "state": "not-fixed"},
                },
                "related_vulnerabilities": [
                    {
                        "id": "CVE-2023-33201",
                        "data_source": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201",
                        "urls": [
                            "https://bouncycastle.org",
                        ],
                        "description": "test",
                        "severity": "Medium",
                        "cvss": [],
                    }
                ],
                "artifact": {
                    "id": "8c3c2be831ddeb02",
                    "version": "1.68",
                    "purl": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                },
            },
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="no-ratings-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "references": [
                        {
                            "id": "GHSA-hr8g-6v94-x4m9",
                            "source": {
                                "url": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                                "name": "Github Advisories",
                            },
                        }
                    ],
                    "ratings": [
                        {
                            "severity": "medium",
                            "method": "other",
                        }
                    ],
                    "description": "test",
                    "recommendation": "State: not-fixed | Fix Versions: ",
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                }
            ),
            {
                "vulnerability": {
                    "id": "GHSA-hr8g-6v94-x4m9",
                    "data_source": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                    "namespace": "github:language:java",
                    "urls": ["https://github.com/advisories/GHSA-hr8g-6v94-x4m9"],
                    "description": "test",
                    "severity": "Medium",
                    "cvss": [
                        {
                            "version": "1.0",
                            "vector": "gibberish",
                            "metrics": {"base_score": 5.8, "exploitability_score": 8.6, "impact_score": 4.9},
                            "vendor_metadata": {"base_severity": "Medium", "status": "N/A"},
                        }
                    ],
                    "fix": {"versions": [], "state": "not-fixed"},
                },
                "related_vulnerabilities": [
                    {
                        "id": "CVE-2023-33201",
                        "data_source": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201",
                        "urls": [
                            "https://bouncycastle.org",
                        ],
                        "description": "test",
                        "severity": "Medium",
                        "cvss": [
                            {
                                "source": "nvd@nist.gov",
                                "type": "Primary",
                                "version": "1.0",
                                "vector": "gibberish",
                                "metrics": {"base_score": 5.8, "exploitability_score": 8.6, "impact_score": 4.9},
                            }
                        ],
                    }
                ],
                "artifact": {
                    "id": "8c3c2be831ddeb02",
                    "version": "1.68",
                    "purl": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                },
            },
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="bad-rating-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "references": [
                        {
                            "id": "GHSA-hr8g-6v94-x4m9",
                            "source": {
                                "url": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                                "name": "Github Advisories",
                            },
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.3,
                            "severity": "medium",
                            "method": "CVSSv31",
                            "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                        }
                    ],
                    "description": "test",
                    "recommendation": "State: not-fixed | Fix Versions: ",
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on?type=jar",
                        }
                    ],
                }
            ),
            {
                "vulnerability": {
                    "id": "GHSA-hr8g-6v94-x4m9",
                    "data_source": "https://github.com/advisories/GHSA-hr8g-6v94-x4m9",
                    "namespace": "github:language:java",
                    "urls": ["https://github.com/advisories/GHSA-hr8g-6v94-x4m9"],
                    "description": "test",
                    "severity": "Medium",
                    "cvss": [
                        {
                            "version": "3.1",
                            "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                            "metrics": {"base_score": 5.3, "exploitability_score": 3.9, "impact_score": 1.4},
                            "vendor_metadata": {"base_severity": "Medium", "status": "N/A"},
                        }
                    ],
                    "fix": {"versions": [], "state": "not-fixed"},
                },
                "related_vulnerabilities": [
                    {
                        "id": "CVE-2023-33201",
                        "data_source": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201",
                        "urls": [
                            "https://bouncycastle.org",
                        ],
                        "description": "test",
                        "severity": "Medium",
                        "cvss": [
                            {
                                "source": "nvd@nist.gov",
                                "type": "Primary",
                                "version": "3.1",
                                "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                                "metrics": {"base_score": 5.3, "exploitability_score": 3.9, "impact_score": 1.4},
                            }
                        ],
                    }
                ],
                "artifact": {
                    "id": "8c3c2be831ddeb02",
                    "purl": "pkg:maven/org.bouncycastle/bcprov-jdk15on?type=jar",
                },
            },
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on?type=jar"],
            id="purl-without-version",
        ),
    ],
    indirect=["grype_match"],
)
def test__convert_to_cyclone_dx(
    expected: Vulnerability,
    grype_match: Match,
    purls_list: list[str],
    scanner: GrypeScanner,
):
    actual = scanner._convert_to_cyclone_dx(grype_match, purls_list)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "os_distro", "sbom", "generic_response"],
    argvalues=[
        pytest.param(
            [
                "GHSA-mvr2-9pj6-7w5j",
                "GHSA-7g45-4rm6-3mm3",
                "GHSA-5mg8-w23w-74h3",
                "GHSA-g6ph-x5wf-g337",
                "GHSA-jcwr-x25h-x5fh",
                "GHSA-mjmj-j48q-9wg2",
            ],
            None,
            "bom/unit_syft_java_bom.json",
            "grype/unit_grype_java_result.json",
            id="basic-vulnsforbom",
        ),
        pytest.param(
            [
                "GHSA-mvr2-9pj6-7w5j",
                "GHSA-7g45-4rm6-3mm3",
                "GHSA-5mg8-w23w-74h3",
                "GHSA-g6ph-x5wf-g337",
                "GHSA-jcwr-x25h-x5fh",
                "GHSA-mjmj-j48q-9wg2",
            ],
            "Ubuntu:20.04.4",
            "bom/unit_syft_java_bom.json",
            "grype/unit_grype_java_result.json",
            id="os_distro-vulnsforbom",
        ),
        pytest.param(
            ["GHSA-mvr2-9pj6-7w5j", "GHSA-7g45-4rm6-3mm3", "GHSA-5mg8-w23w-74h3"],
            "",
            "bom/unit_encoded_purl_bom.json",
            "grype/unit_grype_encoded_result.json",
            id="encoded-purl-vulnsforbom",
        ),
        pytest.param(
            [],
            "",
            "bom/unit_types_npm_bom.json",
            "grype/unit_grype_types_result.json",
            id="npm-types-vulnsforbom",
        ),
    ],
    indirect=["sbom", "generic_response"],
)
def test_get_vulnerabilities_for_sbom(
    expected: list[str],
    os_distro: str | None,
    sbom: Sbom,
    generic_response: tuple[bytes, bytes],
    scanner: GrypeScanner,
    popen_patch: type[Popen],
    monkeypatch: MonkeyPatch,
):
    def convert_to_cyclone_dx_patch(match: Match, _) -> Vulnerability:
        return Vulnerability(id=match.vulnerability.id)

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="_convert_to_cyclone_dx", value=convert_to_cyclone_dx_patch)
        patch.setattr(target=hopprcop.grype.grype_scanner, name="Popen", value=popen_patch)
        scanner.grype_os_distro = os_distro

        actual = scanner.get_vulnerabilities_for_sbom(sbom)

        assert len(actual) == len(expected)
        actual_vulns = [vuln.id for vuln in actual]
        actual_vulns.sort()
        expected.sort()
        assert actual_vulns == expected


@pytest.mark.parametrize(
    argnames=["expected", "os_distro", "sbom", "generic_response"],
    argvalues=[
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "GHSA-mvr2-9pj6-7w5j",
                    "GHSA-7g45-4rm6-3mm3",
                    "GHSA-5mg8-w23w-74h3",
                ],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": ["GHSA-g6ph-x5wf-g337", "GHSA-jcwr-x25h-x5fh"],
                "pkg:maven/org.yaml/snakeyaml@1.33": ["GHSA-mjmj-j48q-9wg2"],
            },
            "",
            "bom/unit_syft_java_bom.json",
            "grype/unit_grype_java_result.json",
            id="basic-vulnsbybom",
        ),
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "GHSA-mvr2-9pj6-7w5j",
                    "GHSA-7g45-4rm6-3mm3",
                    "GHSA-5mg8-w23w-74h3",
                ],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": ["GHSA-g6ph-x5wf-g337", "GHSA-jcwr-x25h-x5fh"],
                "pkg:maven/org.yaml/snakeyaml@1.33": ["GHSA-mjmj-j48q-9wg2"],
            },
            "Ubuntu:20.04.4",
            "bom/unit_syft_java_bom.json",
            "grype/unit_grype_java_result.json",
            id="os_distro-vulnsbybom",
        ),
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "GHSA-mvr2-9pj6-7w5j",
                    "GHSA-7g45-4rm6-3mm3",
                    "GHSA-5mg8-w23w-74h3",
                ],
                "pkg:deb/debian/linux-libc-dev@4.19.235-1?arch=amd64\u0026upstream=linux\u0026distro=debian-10": [],
            },
            "",
            "bom/unit_encoded_purl_bom.json",
            "grype/unit_grype_encoded_result.json",
            id="encoded-purl-vulnsbybom",
        ),
    ],
    indirect=["sbom", "generic_response"],
)
def test_get_vulnerabilities_by_sbom(
    expected: dict[str, list[str]],
    os_distro: str,
    sbom: Sbom,
    generic_response: tuple[bytes, bytes],
    scanner: GrypeScanner,
    popen_patch: type[Popen],
    monkeypatch: MonkeyPatch,
):
    def convert_to_cyclone_dx_patch(match: Match) -> str:
        return match.vulnerability.id

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner, name="_convert_to_cyclone_dx_deprecated", value=convert_to_cyclone_dx_patch)
        patch.setattr(target=hopprcop.grype.grype_scanner, name="Popen", value=popen_patch)
        scanner.grype_os_distro = os_distro

        actual = scanner.get_vulnerabilities_by_sbom(sbom)

        assert len(actual) == len(expected)
        for purl in actual:
            actual[purl] = sorted(actual[purl])  # type: ignore[type-var]
            expected[purl] = sorted(expected[purl])
            assert actual[purl] == expected[purl]
