"""Shareable fixtures for unit tests."""

from __future__ import annotations

import typing

from pathlib import Path
from subprocess import Popen
from typing import IO, Any, TypeAlias

import pytest

from hoppr import Affect, Component, ComponentType, Sbom, Tools, Vulnerability, cdx
from typer.testing import CliRunner
from typing_extensions import Self

import hopprcop

from hopprcop.gemnasium.gemnasium_scanner import GemnasiumScanner
from hopprcop.grype.grype_scanner import GrypeScanner
from hopprcop.ossindex.oss_index_scanner import OSSIndexScanner
from hopprcop.trivy.trivy_scanner import TrivyScanner
from hopprcop.vulnerability_enhancer import BaseEnhancer
from hopprcop.vulnerability_scanner import VulnerabilitySuper


Config: TypeAlias = pytest.Config
FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(autouse=True)
def _print_test_name_fixture(request: FixtureRequest):
    """Print name of test."""
    print(f"-------- Starting {request.node.name} --------")  # noqa: T201


@pytest.fixture(name="generic_response")
def generic_response_fixture(request: FixtureRequest, resources_dir: Path) -> tuple[bytes, bytes]:
    relative_path = getattr(request, "result_file", Path("general"))
    gen_file = resources_dir / relative_path
    return (gen_file.read_bytes(), b"error response")


@pytest.fixture(name="return_code")
def return_code_fixture(request: FixtureRequest) -> int:
    return getattr(request, "param", 0)


@pytest.fixture
def hoppr_cop_runner() -> CliRunner:
    """Fixture to return `typer.testing.CliRunner`."""
    return CliRunner()


@pytest.fixture(name="popen_patch")
def popen_patch_fixture(generic_response: tuple[bytes, str], return_code: int) -> type[Popen]:
    """Patch Popen class to not actually depend on the binaries."""

    class PatchedPopen(Popen):
        def __init__(
            self,
            args: list[str],
            stdout: IO[Any] | None = None,
            stdin: IO[Any] | None = None,
            stderr: IO[Any] | None = None,
            env: dict[str, str] | None = None,
            text: bool | None = None,
        ):
            self.args = args
            self.stdout = stdout
            self.stdin = stdin
            self.stderr = stderr
            self.env = env
            self.text = text

        def __enter__(self) -> Self:
            return self

        def __exit__(self, *_, **__):
            return None

        def communicate(
            self, input: bytes | str | None = None, timeout: float | None = None
        ) -> tuple[bytes | str, bytes | str]:
            return generic_response

        returncode = return_code

    return PatchedPopen


@pytest.fixture(name="sbom")
def sbom_fixture(resources_dir: Path, request: FixtureRequest) -> Sbom:
    sbom_file = getattr(request, "param", Path("bom") / "unit_syft_java_bom.json")

    return Sbom() if sbom_file == "empty" else Sbom.load(resources_dir / sbom_file)


@pytest.fixture(autouse=True)
def _should_activate_fixture(request: FixtureRequest, monkeypatch: MonkeyPatch):
    """Patch should_activate for all scanners, so they will always be active for tests."""
    param_dict = dict(getattr(request, "param", {}))
    param_dict = {
        "GemnasiumScanner": True,
        "GrypeScanner": True,
        "OSSIndexScanner": True,
        "TrivyScanner": True,
    } | param_dict

    for scanner in GemnasiumScanner, GrypeScanner, OSSIndexScanner, TrivyScanner:
        if should_patch := param_dict[scanner.__name__]:
            monkeypatch.setattr(target=scanner, name="should_activate", value=lambda _: should_patch)


@pytest.fixture(name="scanner")
def scanner_fixture(request: FixtureRequest) -> VulnerabilitySuper:
    """Fixture to return scanner object based on type hint."""
    try:
        # Get type of scanner to return from test function's type hints
        scanner_cls = typing.get_type_hints(request.function)["scanner"]
        assert issubclass(scanner_cls, VulnerabilitySuper), f"{scanner_cls}"

        scanner_obj = scanner_cls()
    except KeyError as ex:
        raise TypeError("Type hint must be provided in order to use scanner_fixture.") from ex
    except AssertionError as ex:
        raise TypeError(f"Type hint provided to scanner_fixture not a subclass of VulnerabilitySuper: '{ex}'") from ex

    return scanner_obj


@pytest.fixture(name="enhancer")
def enhancer_fixture(request: FixtureRequest) -> BaseEnhancer:
    """Fixture to return scanner object based on type hint."""
    try:
        # Get type of enhancer to return from test function's type hints
        enhancer_cls = typing.get_type_hints(request.function)["enhancer"]
        assert issubclass(enhancer_cls, BaseEnhancer), f"{enhancer_cls}"

        enhancer_obj = enhancer_cls()
    except KeyError as ex:
        raise TypeError("Type hint must be provided in order to use enhancer_fixture.") from ex
    except AssertionError as ex:
        raise TypeError(f"Type hint provided to enhancer_fixture not a subclass of VulnerabilitySuper: '{ex}'") from ex

    return enhancer_obj


@pytest.fixture(name="resources_dir", scope="session")
def resources_dir_fixture(pytestconfig: Config) -> Path:
    """Fixture to return Path object representing test/resources directory."""
    return pytestconfig.rootpath / "test" / "resources"


@pytest.fixture(name="ratings")
def ratings_fixture(request: FixtureRequest) -> list[cdx.Rating] | None:
    if rating_list := getattr(
        request,
        "param",
        [
            {
                "score": "5.9",
                "severity": "medium",
                "method": "CVSSv31",
                "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:N",
            }
        ],
    ):
        return [cdx.Rating.parse_obj(rating) for rating in rating_list]
    else:
        return None


@pytest.fixture(name="advisories")
def advisories_fixture(request: FixtureRequest) -> list[cdx.Advisory]:
    advisory_list = getattr(request, "param", [{"url": "https://fake-url"}])

    return [cdx.Advisory.parse_obj(advisory) for advisory in advisory_list]


@pytest.fixture(name="affects")
def affects_fixture(request: FixtureRequest) -> list[Affect]:
    affects_list = getattr(request, "param", [{"ref": "pkg:golang/golang.org/x/crypto@v0.14.0"}])

    return [Affect.parse_obj(affect) for affect in affects_list]


@pytest.fixture(name="tools")
def tools_fixture() -> Tools:
    return Tools(
        components=[
            Component(
                type=ComponentType.APPLICATION,
                supplier=cdx.OrganizationalEntity(name="Hoppr"),
                name="HopprCop",
                version=hopprcop.__version__,
                scope=cdx.Scope.EXCLUDED,
            )
        ]
    )


@pytest.fixture(name="vulnerability")
def vulnerability_fixture(
    request: FixtureRequest,
    ratings: list[cdx.Rating],
    advisories: list[cdx.Advisory],
    affects: list[Affect],
    tools: Tools,
) -> Vulnerability | None:
    param_dict = getattr(request, "param", {})

    return (
        Vulnerability(
            id=param_dict.get("id", "CVE-2024-00000"),
            description="this is a test",
            recommendation="test recomendation",
            ratings=ratings,
            advisories=advisories,
            affects=affects,
            tools=tools,
        )
        if param_dict
        else None
    )


@pytest.fixture(name="vulnerabilities")
def vulnerabilities_fixture(
    request: FixtureRequest,
    ratings: list[cdx.Rating],
    advisories: list[cdx.Advisory],
    affects: list[Affect],
    tools: Tools,
) -> list[Vulnerability]:
    param_dict = getattr(request, "param", [])

    return [
        Vulnerability(
            id=vuln,
            description="this is a test",
            recommendation="test recomendation",
            ratings=ratings,
            advisories=advisories,
            affects=affects,
            tools=tools,
        )
        for vuln in param_dict
    ]
