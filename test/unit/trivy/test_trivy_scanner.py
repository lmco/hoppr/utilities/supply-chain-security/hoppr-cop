from __future__ import annotations

from subprocess import Popen  # noqa: TC003
from typing import TYPE_CHECKING, Any, TypeAlias

import pytest

from hoppr import Component, ComponentType, HopprError, Sbom, Vulnerability
from packageurl import PackageURL  # noqa: TC002

import hopprcop.trivy.trivy_scanner

from hopprcop.trivy.trivy_scanner import TrivyScanner


if TYPE_CHECKING:
    from pathlib import Path


FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture(name="generic_response")
def grype_response_fixture(request: FixtureRequest, resources_dir: Path) -> tuple[bytes, bytes]:
    param_dict = getattr(request, "param", {"path": "trivy/unit_trivy_java_result.json", "error_msg": b"error string"})
    relative_path = param_dict["path"]
    error_msg = param_dict["error_msg"]
    grype_file = resources_dir / relative_path
    return (grype_file.read_bytes(), error_msg)


@pytest.fixture(name="bom_dict")
def bom_dict_fixture(request: FixtureRequest) -> dict[str, Any]:
    components = getattr(request, "param", [])
    bom: dict[str, Any] = {"components": components}
    return bom


@pytest.mark.parametrize(
    argnames=["offline", "os_distro"],
    argvalues=[
        pytest.param(False, None, id="default-init"),
        pytest.param(True, None, id="offline-init"),
        pytest.param(False, "Debian", id="os-init"),
        pytest.param(True, "RHEL", id="os-offline-init"),
    ],
)
def test_init(
    offline: bool,
    os_distro: str | None,
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setenv(name="OS_DISTRIBUTION", value="ENV_OS")

        actual = TrivyScanner(offline, os_distro)

        assert actual.trivy_os_distro == (os_distro or "ENV_OS")
        assert actual.offline_mode == offline


@pytest.mark.parametrize(
    argnames=["expected", "expected_out", "generic_response", "return_code"],
    argvalues=[
        pytest.param(
            True,
            "TrivyScanner: database downloaded",
            {
                "path": "trivy/unit_trivy_java_result.json",
                "error_msg": b"2024-01-25T13:29:26.459Z\t\x1b[34mINFO\x1b[0m\tNeed to update DB\n"
                b"2024-01-25T13:29:26.459Z\t\x1b[34mINFO\x1b[0m\tDB Repository: ghcr.io/aquasecurity/trivy-db\n"
                b"2024-01-25T13:29:26.459Z\t\x1b[34mINFO\x1b[0m\tDownloading DB...\n",
            },
            0,
            id="successful-db",
        ),
        pytest.param(
            True,
            "TrivyScanner: No update needed for database",
            {
                "path": "trivy/unit_trivy_java_result.json",
                "error_msg": b"",
            },
            0,
            id="no-update-db",
        ),
        pytest.param(
            False,
            "TrivyScanner: Error occurred while updating the database",
            {
                "path": "trivy/unit_trivy_java_result.json",
                "error_msg": b"2024-01-25T13:38:09.077Z\t\x1b[34mINFO\x1b[0m\tNeed to update DB\n"
                b"2024-01-25T13:38:09.078Z\t\x1b[34mINFO\x1b[0m\tDB Repository: broken\n"
                b"2024-01-25T13:38:09.078Z\t\x1b[34mINFO\x1b[0m\tDownloading DB...\n"
                b"2024-01-25T13:38:09.666Z\t\x1b[31mFATAL\x1b[0m\tinit error: "
                b"DB error: failed to download vulnerability DB: database download error: OCI repository error: "
                b"1 error occurred:\n\t* GET https://index.docker.io/v2/library/broken/manifests/2: "
                b"UNAUTHORIZED: authentication required; "
                b"[map[Action:pull Class: Name:library/broken Type:repository]]\n\n\n",
            },
            1,
            id="error-db",
        ),
    ],
    indirect=["generic_response", "return_code"],
)
def test_get_vulnerability_db(
    expected: bool,
    expected_out: str,
    generic_response: tuple[bytes, bytes],
    return_code: int,
    scanner: TrivyScanner,
    popen_patch: type[Popen],
    capsys: pytest.CaptureFixture[str],
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.trivy.trivy_scanner, name="Popen", value=popen_patch)

        actual = scanner.get_vulnerability_db()

        out, error = capsys.readouterr()
        output_lines = list(filter(None, out.split("\n")))

        assert actual == expected
        assert expected_out in output_lines


@pytest.mark.parametrize(
    argnames=["expected", "bom_dict", "os_distro", "os_component"],
    argvalues=[
        pytest.param(
            [
                {
                    "bom-ref": "ab16d2bb-90f7-4049-96ce-8c473ba13bd2",
                    "type": "operating-system",
                    "name": "debian",
                    "version": "12",
                }
            ],
            [],
            None,
            Component(type=ComponentType.OPERATING_SYSTEM, name="debian", version="12"),
            id="basic-addoscomp",
        ),
        pytest.param(
            [
                {
                    "bom-ref": "ab16d2bb-90f7-4049-96ce-8c473ba13bd2",
                    "type": "operating-system",
                    "name": "ubuntu",
                    "version": "24.04",
                }
            ],
            [],
            "ubuntu:24.04",
            None,
            id="os-distro-addoscomp",
        ),
        pytest.param(
            [],
            [],
            "has:an:extra",
            None,
            id="invalid-os-distro-addoscomp",
        ),
        pytest.param(
            [],
            [],
            None,
            None,
            id="None-addoscomp",
        ),
    ],
    indirect=["bom_dict"],
)
def test__add_operating_system_component(
    expected: list[dict[str, str]],
    bom_dict: dict[str, Any],
    os_distro: str | None,
    os_component: Component | None,
    scanner: TrivyScanner,
):
    scanner.trivy_os_distro = os_distro
    scanner._os_component = os_component
    scanner._add_operating_system_component(bom_dict)

    bom_dict["components"] = sorted(bom_dict["components"])
    expected = sorted(expected)  # type: ignore[type-var]

    assert len(bom_dict["components"]) == len(expected)
    assert bom_dict["components"] == expected


def test_get_vulnerabilities_by_purl():
    ...


@pytest.mark.parametrize(
    argnames=["expected", "sbom", "generic_response"],
    argvalues=[
        pytest.param(
            ["CVE-2018-10237", "CVE-2023-2976", "CVE-2020-8908", "CVE-2022-4244", "CVE-2022-4245", "CVE-2022-1471"],
            "bom/unit_syft_java_bom.json",
            {"path": "trivy/unit_trivy_java_result.json", "error_msg": "error string"},
            id="basic-vulnsforsbom",
        ),
        pytest.param(
            ["CVE-2022-1471"],
            "bom/unit_bad_purl_bom.json",
            {"path": "trivy/unit_trivy_bad_result.json", "error_msg": "error string"},
            id="bad-purl-vulnsforsbom",
        ),
        pytest.param(
            [],
            "bom/unit_bad_purl_bom.json",
            {"path": "trivy/unit_trivy_no_rating_result.json", "error_msg": "error string"},
            id="no-rating-vulnsforsbom",
        ),
    ],
    indirect=["sbom", "generic_response"],
)
def test_get_vulnerabilities_for_sbom(
    expected: list[str],
    sbom: Sbom,
    scanner: TrivyScanner,
    generic_response: tuple[bytes, bytes],
    popen_patch: type[Popen],
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.trivy.trivy_scanner, name="Popen", value=popen_patch)

        actual: list[Vulnerability] = scanner.get_vulnerabilities_for_sbom(sbom)

        assert len(actual) == len(expected)
        actual_vulns = [vuln.id for vuln in actual]
        actual_vulns.sort()
        expected.sort()
        assert actual_vulns == expected


@pytest.mark.parametrize(
    argnames=["expected", "sbom", "generic_response"],
    argvalues=[
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "CVE-2018-10237",
                    "CVE-2023-2976",
                    "CVE-2020-8908",
                ],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": ["CVE-2022-4244", "CVE-2022-4245"],
                "pkg:maven/org.yaml/snakeyaml@1.33": ["CVE-2022-1471"],
            },
            "bom/unit_syft_java_bom.json",
            {"path": "trivy/unit_trivy_java_result.json", "error_msg": "error string"},
            id="basic-vulnsbysbom",
        ),
        pytest.param(
            {
                "pkg:maven/org.yaml/snakeyaml@1.33": ["CVE-2022-1471"],
            },
            "bom/unit_bad_purl_bom.json",
            {"path": "trivy/unit_trivy_bad_result.json", "error_msg": "error string"},
            id="bad-purl-vulnsbysbom",
        ),
        pytest.param(
            {},
            "bom/unit_bad_purl_bom.json",
            {"path": "trivy/unit_trivy_no_rating_result.json", "error_msg": "error string"},
            id="no-rating-vulnsbysbom",
        ),
    ],
    indirect=["sbom", "generic_response"],
)
def test_get_vulnerabilities_by_sbom(
    expected: dict[str, list[str]],
    sbom: Sbom,
    scanner: TrivyScanner,
    generic_response: tuple[bytes, bytes],
    popen_patch: type[Popen],
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.trivy.trivy_scanner, name="Popen", value=popen_patch)

        actual: dict[str, list[Vulnerability]] = scanner.get_vulnerabilities_by_sbom(sbom)

        assert len(actual) == len(expected)
        for purl in actual:
            assert len(actual[purl]) == len(expected[purl])
            actual_vulns = [cve.id for cve in actual[purl]]
            actual_vulns.sort()
            expected[purl].sort()
            assert actual_vulns == expected[purl]


@pytest.mark.parametrize(
    argnames=["expected", "sbom", "generic_response"],
    argvalues=[
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "CVE-2018-10237",
                    "CVE-2023-2976",
                    "CVE-2020-8908",
                ],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": ["CVE-2022-4244", "CVE-2022-4245"],
                "pkg:maven/org.yaml/snakeyaml@1.33": ["CVE-2022-1471"],
            },
            "bom/unit_syft_java_bom.json",
            {"path": "trivy/unit_trivy_error_result.json", "error_msg": "error string"},
            id="basic-vulnsbysbom-error",
        ),
    ],
    indirect=["sbom", "generic_response"],
)
def test_get_vulnerabilities_by_sbom_error(
    expected: list[PackageURL],
    sbom: Sbom,
    scanner: TrivyScanner,
    generic_response: tuple[bytes, bytes],
    popen_patch: type[Popen],
    monkeypatch: MonkeyPatch,
):
    with monkeypatch.context() as patch:
        patch.setattr(target=hopprcop.trivy.trivy_scanner, name="Popen", value=popen_patch)

        with pytest.raises(HopprError):
            scanner.get_vulnerabilities_by_sbom(sbom)
