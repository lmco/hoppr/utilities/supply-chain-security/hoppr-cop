from __future__ import annotations

import json

from pathlib import Path
from typing import TYPE_CHECKING

import pytest
import rich

from hoppr import Vulnerability
from packageurl import PackageURL

from hopprcop.ossindex.api.model import Vulnerability as OssVulnerability
from hopprcop.ossindex.api.serializer import json_decoder
from hopprcop.ossindex.oss_index_scanner import OSSIndexScanner  # noqa: TC001


if TYPE_CHECKING:
    from hopprcop.ossindex.api.model import OssIndexComponent


MonkeyPatch = pytest.MonkeyPatch


@pytest.fixture(name="oss_vulnerability")
def oss_vulnerability_fixture(request: bytes) -> OssVulnerability:
    oss_result = getattr(request, "param", {"url": "https://fake-url"})

    return OssVulnerability(
        id_=oss_result.get("id", "CVE-2020-8908"),
        title=f"{oss_result.get('name', 'CVE-2020-8908')} title line",
        cve=oss_result.get("id", "CVE-2020-8908"),
        display_name=oss_result.get("name", "CVE-2020-8908"),
        description=oss_result.get("description", "this is a test"),
        cwe=oss_result.get("cwe", "CWE-379"),
        cvss_score=oss_result.get("score", 3.3),
        cvss_vector=oss_result.get("cvss_vector", "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N"),
        external_references=oss_result.get("external_references", ["https://github.com/google/guava/issues/4011"]),
        reference="https://fake-url.test/fake-oss-index",
    )


@pytest.mark.parametrize(
    argnames=["expected", "oss_vulnerability", "purl", "purls_list"],
    argvalues=[
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "description": "this is a test",
                    "cwes": [379],
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.3,
                            "severity": "medium",
                            "method": "CVSSv3",
                            "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                        }
                    ],
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                }
            ),
            {
                "id": "CVE-2023-33201",
                "name": "CVE-2023-33201",
                "cvss_score": 5.3,
                "cvss_vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                "external_references": ["https://bouncycastle.org"],
            },
            "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="basic-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "description": "this is a test",
                    "cwes": [379],
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.8,
                            "severity": "medium",
                            "method": "CVSSv2",
                            "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                        }
                    ],
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                }
            ),
            {
                "id": "CVE-2023-33201",
                "name": "CVE-2023-33201",
                "cvss_score": 5.8,
                "cvss_vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                "external_references": ["https://bouncycastle.org"],
            },
            "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="cvss2-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "description": "this is a test",
                    "cwes": [],
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.3,
                            "severity": "medium",
                            "method": "CVSSv3",
                            "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                        }
                    ],
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                }
            ),
            {
                "id": "CVE-2023-33201",
                "name": "CVE-2023-33201",
                "cwe": None,
                "cvss_score": 5.3,
                "cvss_vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N",
                "external_references": ["https://bouncycastle.org"],
            },
            "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="no-cwes-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "description": "this is a test",
                    "cwes": [379],
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                }
            ),
            {
                "id": "CVE-2023-33201",
                "name": "CVE-2023-33201",
                "cvss_score": 5.3,
                "cvss_vector": None,
                "external_references": ["https://bouncycastle.org"],
            },
            "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="no-cvss-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "description": "this is a test",
                    "cwes": [379],
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.8,
                            "severity": "medium",
                            "method": "CVSSv4",
                            "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                        }
                    ],
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                }
            ),
            {
                "id": "CVE-2023-33201",
                "name": "CVE-2023-33201",
                "cvss_score": 5.8,
                "cvss_vector": "CVSS:4.0/AV:L/AC:L/AT:P/PR:L/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
                "external_references": ["https://bouncycastle.org"],
            },
            "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="cvss4-convert-to-cdx",
        ),
        pytest.param(
            Vulnerability.parse_obj(
                {
                    "id": "CVE-2023-33201",
                    "description": "this is a test",
                    "cwes": [379],
                    "source": {"url": "https://nvd.nist.gov/vuln/detail/CVE-2023-33201", "name": "NVD"},
                    "affects": [
                        {
                            "ref": "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
                            "versions": [{"version": "1.68", "status": "affected"}],
                        }
                    ],
                    "ratings": [
                        {
                            "score": 5.8,
                            "severity": "medium",
                            "method": "CVSSv2",
                            "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:N",
                        }
                    ],
                    "advisories": [
                        {"url": "https://bouncycastle.org"},
                    ],
                    "tools": {
                        "components": [
                            {
                                "type": "application",
                                "supplier": {"name": "Anchore"},
                                "name": "Grype",
                                "scope": "required",
                            }
                        ],
                    },
                }
            ),
            {
                "id": "CVE-2023-33201",
                "name": "CVE-2023-33201",
                "cvss_score": 5.8,
                "cvss_vector": "CVSS:UNKNOWN/AV:N/AC:M/Au:N/C:P/I:P/A:N",
                "external_references": ["https://bouncycastle.org"],
            },
            "pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar",
            ["pkg:maven/org.bouncycastle/bcprov-jdk15on@1.68?type=jar"],
            id="unknown-cvss-convert-to-cdx",
        ),
    ],
    indirect=["oss_vulnerability"],
)
def test__convert_to_cyclone_dx(
    expected: Vulnerability,
    oss_vulnerability: OssVulnerability,
    purl: str,
    purls_list: list[str],
    scanner: OSSIndexScanner,
):
    actual = scanner._convert_to_cyclone_dx(oss_vulnerability, purl, purls_list)

    assert actual == expected


@pytest.mark.parametrize(
    argnames=["expected", "purls"],
    argvalues=[
        pytest.param(
            ["CVE-2018-10237", "CVE-2020-8908", "CVE-2023-2976"],
            ["pkg:maven/com.google.guava/guava@16.0.1"],
            id="basic-vulnforpurl",
        ),
        pytest.param(
            ["CVE-2018-10237", "CVE-2020-8908", "CVE-2023-2976", "CVE-2022-1471"],
            [
                "pkg:maven/com.google.guava/guava@16.0.1",
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20",
                "pkg:maven/org.yaml/snakeyaml@1.33",
            ],
            id="multiple-purls-vulnforpurl",
        ),
        pytest.param(
            [],
            ["pkg:rpm/rhel/curl@7.61.1-33.el8?arch=x86_64&upstream=curl-7.61.1-33.el8.src.rpm&distro=rhel-8.9"],
            id="rpm-purl-vulnforpurl",
        ),
        pytest.param(
            [],
            [
                "pkg:maven/not.a.real/artifact@0.0.0",
            ],
            id="purl-notfound-vulnforpurl",
        ),
        pytest.param(
            [],
            [
                "pkg:maven/not.a.real/artifact",
            ],
            id="purl-missing-version",
        ),
        pytest.param(
            ["CVE-2018-10237", "CVE-2020-8908", "CVE-2023-2976", "CVE-2022-1471"],
            [
                "pkg:maven/com.google.guava/guava@16.0.1",
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20",
                "pkg:maven/org.yaml/snakeyaml@1.33",
                "pkg:maven/not.a.real/artifact",
                "pkg:maven/not.a.real/artifact2",
            ],
            id="filter-out-purls-without-version",
        ),
    ],
)
def test_get_vulnerabilities_for_purl(
    expected: list[str],
    purls: list[str],
    resources_dir: Path,
    scanner: OSSIndexScanner,
    monkeypatch: MonkeyPatch,
):
    def convert_to_cyclone_dx_patch(vulnerability: Vulnerability, _, __) -> Vulnerability:
        return Vulnerability(id=vulnerability.id)

    def get_component_report_patch(packages: list[PackageURL]) -> list[OssIndexComponent]:
        full_report: list[OssIndexComponent] = []
        search_path = ""
        for purl in packages:
            search_path = f"{search_path}{purl.name}{purl.version}_"

        report_path = Path(
            resources_dir / "ossindex" / "component_reports" / search_path.strip("_") / "component-report.json"
        )

        if report_path.is_file():
            full_report.extend(list(json.loads(report_path.read_text(encoding="utf-8"), object_hook=json_decoder)))

        return full_report

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner.api, name="get_component_report", value=get_component_report_patch)
        patch.setattr(target=scanner, name="_convert_to_cyclone_dx", value=convert_to_cyclone_dx_patch)

        actual = scanner.get_vulnerabilities_for_purl(purls)

        assert len(actual) == len(expected), f"{len(actual)} != {len(expected)}"
        actual_vulns = [vuln.id for vuln in actual]
        actual_vulns.sort()
        expected.sort()
        assert actual_vulns == expected


@pytest.mark.parametrize(
    argnames=["expected", "purls"],
    argvalues=[
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "CVE-2018-10237",
                    "CVE-2020-8908",
                    "CVE-2023-2976",
                ]
            },
            [PackageURL.from_string("pkg:maven/com.google.guava/guava@16.0.1")],
            id="basic-vulnbypurl",
        ),
        pytest.param(
            {
                "pkg:maven/com.google.guava/guava@16.0.1": [
                    "CVE-2018-10237",
                    "CVE-2020-8908",
                    "CVE-2023-2976",
                ],
                "pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20": [],
                "pkg:maven/org.yaml/snakeyaml@1.33": ["CVE-2022-1471"],
            },
            [
                PackageURL.from_string("pkg:maven/com.google.guava/guava@16.0.1"),
                PackageURL.from_string("pkg:maven/org.codehaus.plexus/plexus-utils@3.0.20"),
                PackageURL.from_string("pkg:maven/org.yaml/snakeyaml@1.33"),
            ],
            id="multiple-purls-vulnbypurl",
        ),
        pytest.param(
            {},
            [
                PackageURL.from_string(
                    "pkg:rpm/rhel/curl@7.61.1-33.el8?arch=x86_64&upstream=curl-7.61.1-33.el8.src.rpm&distro=rhel-8.9"
                )
            ],
            id="rpm-purl-vulnbypurl",
        ),
        pytest.param(
            {},
            [
                PackageURL.from_string("pkg:maven/not.a.real/artifact@0.0.0"),
            ],
            id="purl-notfound-vulnbypurl",
        ),
    ],
)
def test_get_vulnerabilities_by_purl(
    expected: dict[str, list[str]],
    purls: list[PackageURL],
    resources_dir: Path,
    scanner: OSSIndexScanner,
    monkeypatch: MonkeyPatch,
):
    def convert_to_cyclone_dx_patch(vulnerability: Vulnerability) -> Vulnerability:
        return Vulnerability(id=vulnerability.id)

    def get_component_report_patch(packages: list[PackageURL]) -> list[OssIndexComponent]:
        full_report: list[OssIndexComponent] = []
        search_path = ""
        for purl in packages:
            search_path = f"{search_path}{purl.name}{purl.version}_"

        report_path = Path(
            resources_dir / "ossindex" / "component_reports" / search_path.strip("_") / "component-report.json"
        )

        if report_path.is_file():
            full_report.extend(list(json.loads(report_path.read_text(encoding="utf-8"), object_hook=json_decoder)))

        return full_report

    with monkeypatch.context() as patch:
        patch.setattr(target=scanner.api, name="get_component_report", value=get_component_report_patch)
        patch.setattr(target=scanner, name="_convert_to_cyclone_dx_deprecated", value=convert_to_cyclone_dx_patch)

        actual = scanner.get_vulnerabilities_by_purl(purls)
        rich.print(actual)

        assert len(actual) == len(expected)
        for purl in expected:
            actual_vulns = [vuln.id for vuln in actual[purl]]
            actual_vulns.sort()
            expected[purl].sort()
            assert actual_vulns == expected[purl]
