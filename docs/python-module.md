# Use as Python Library

This library can also be used programmatically, here is an example of interacting with the combined scanner from python code.

```python
from pathlib import Path

from hoppr_security_commons.reporting.models import ReportFormat
from hoppr_security_commons.reporting import Reporting
from hoppr_security_commons.utils import parse_sbom
from hopprcop.combined.combined_scanner import CombinedScanner
from hopprcop.gemnasium.gemnasium_scanner import GemnasiumScanner
from hopprcop.grype.grype_scanner import GrypeScanner
from hopprcop.trivy.trivy_scanner import TrivyScanner
from hopprcop.ossindex.oss_index_scanner import OSSIndexScanner

output_dir = Path("./reports")
bom = Path("bom.json")
formats = [ReportFormat.HTML]

reporting = Reporting(output_dir, bom.name.removesuffix(".json"))
combined = CombinedScanner()
combined.set_scanners([GemnasiumScanner(), GrypeScanner(), TrivyScanner(), OSSIndexScanner()])
parsed_bom = parse_sbom(bom)
result = combined.get_vulnerabilities_for_sbom(parsed_bom)
reporting.generate_vulnerability_reports(formats, result, parsed_bom)
```

## Words of caution

It is strongly advised to stick closely to the above template to ensure that your implementation continues working. The python library use case should only be used in a situation where the CLI does not meet your needs. If that is the case then make sure that you stick closely to the CLI implementation to avoid any potential problems.

Also note that you should not access any private methods as their functionality outside of the core usage is not guaranteed. This are denoted by the presence of a leading `_`, i.e. `_private_method`.

## Deprecations

Please be aware that many methods have been deprecated due to a change in the data structure. The new scanner methods will now return a list of vulnerabilities instead of a mapping of PURLs to a list of vulnerabilities. Below is a python type representation of the change --

```python
# Deprecated return type
dict[str, list[Vulnerability]]

# New method return type
list[Vulnerability]
```

Below is a complete list of deprecated public methods and their replacement methods --

- `CombinedScanner`
    - `get_vulnerabilities_by_purl` switch to `get_vulnerabilities_for_purl`
    - `get_vulnerabilities_by_sbom` switch to `get_vulnerabilities_for_purl`
- `GemnasiumScanner`
    - `get_vulnerabilities_by_purl` switch to `get_vulnerabilities_for_purl`
    - `get_vulnerabilities_by_sbom` switch to `get_vulnerabilities_for_purl`
- `GrypeScanner`
    - `get_vulnerabilities_by_purl` switch to `get_vulnerabilities_for_purl`
    - `get_vulnerabilities_by_sbom` switch to `get_vulnerabilities_for_purl`
- `TrivyScanner`
    - `get_vulnerabilities_by_purl` switch to `get_vulnerabilities_for_purl`
    - `get_vulnerabilities_by_sbom` switch to `get_vulnerabilities_for_purl`
- `OSSIndexScanner`
    - `get_vulnerabilities_by_purl` switch to `get_vulnerabilities_for_purl`
    - `get_vulnerabilities_by_sbom` switch to `get_vulnerabilities_for_purl`
- `Analysis`
    - `check_assessments` switch to `apply_assessments`
