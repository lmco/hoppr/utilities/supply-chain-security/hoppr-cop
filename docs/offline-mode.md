# Offline Mode

The offline mode for HopprCop primarily exists to support air-gapped environments or other situations without access to the internet. If it is possible to use the internet while running HopprCop this is preferable so that vulnerability information is as up to date as possible.

There are two main parts to the offline mode:

<!-- no toc -->
- [Offline Scan](#offline-scan)
- [Database Update](#database-update)

## Important Notes

Not all scanners support a method of running without connection to the internet.

As such it is not possible to use them while HopprCop is offline.

See the table below for scanners support of offline mode:

| Scanner      | Status        |
| ------------ | ------------- |
| Gemmnasium   | Supported     |
| Grype        | Supported     |
| OSS Index    | Not Supported |
| Trivy        | Supported     |

## Offline Scan

The Offline Scan can be enabled by adding the `--offline` flag to the cli command. This will set the scanners to run with the local Vulnerability Databases without attempting to update.

:::note

This scan requires the Vulnerability Databases to be loaded onto the disconnected system for the scans to function. These databases will need to be updated regularly. The more out of date they become the more issues the scans will miss.

:::

Under the hood the Offline Scan sets the scanners to not update the database or validate the age. It is on the end user to ensure that the databases are being regularly updated in the offline environment.

## Database Update

The Database Update can be enabled by adding the `--download-dbs` flag to the cli command. This will run a database update process for each scanner. The database files will be loaded to the scanners cache directory and will need transferred by the user.

This should be ran to prepare the Vulnerability Databases for transfer.

Cache information by scanner:

- Gemnasium Database Cache -- uses the `CACHE_DIR` environment variable with the default being the OS temp location (i.e. `/tmp` on Linux)
- [Grype Database Cache](https://github.com/anchore/grype?tab=readme-ov-file#local-database-cache-directory)
- Trivy Database Cache -- run `trivy --help` and look at the entry for the `--cache-dir` flag
