# EPSS

This feature documents an epss score and probability for a vulnerability.

## CLI

Set the `--epss` option to set an EPSS score in the rating section for a vulnerabilty, if there is one.

![usage](usage.png)

## EPSS Document

See the [First API EPSS](https://api.first.org/epss/) documentation for more details on the EPSS model.

## Example API Response

```json
{
"status":"OK",
"status-code":200,
"version":"1.0",
"access":"public",
"total":1,
"offset":0,
"limit":100,
"data":[{"cve":"CVE-2023-48795","epss":"0.694740000","percentile":"0.979330000","date":"2024-04-01"}]
}
```

## Example Bom

```json
    "vulnerabilities": [
    {
      "id": "CVE-2021-3918",
      "source": {
        "url": "https://nvd.nist.gov/vuln/detail/CVE-2021-3918",
        "name": "NVD"
      },
      "references": [
        {
          "id": "GHSA-896r-f27r-55mw",
          "source": {
            "url": "https://github.com/advisories/GHSA-896r-f27r-55mw",
            "name": "Github Advisories"
          }
        }
      ],
      "ratings": [
        {
          "score": 9.8,
          "severity": "critical",
          "method": "CVSSv31",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "source": {
            "name": "alma"
          },
          "severity": "medium"
        },
        {
          "source": {
            "name": "ghsa"
          },
          "score": 9.8,
          "severity": "critical",
          "method": "CVSSv31",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "source": {
            "name": "nvd"
          },
          "score": 7.5,
          "severity": "high",
          "method": "CVSSv2",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        },
        {
          "source": {
            "name": "nvd"
          },
          "score": 9.8,
          "severity": "critical",
          "method": "CVSSv31",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "source": {
            "name": "oracle-oval"
          },
          "severity": "medium"
        },
        {
          "source": {
            "name": "redhat"
          },
          "score": 9.8,
          "severity": "high",
          "method": "CVSSv31",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "source": {
            "name": "ubuntu"
          },
          "severity": "medium"
        },
        {
          "source": {
            "url": "https://api.first.org/data/v1/epss?cve=CVE-2021-3918",
            "name": "EPSS-Probability"
          },
          "score": 0.00384,
          "method": "other"
        },
        {
          "source": {
            "url": "https://api.first.org/data/v1/epss?cve=CVE-2021-3918",
            "name": "EPSS-Percentile"
          },
          "score": 0.7262,
          "method": "other"
        }
      ],
      "cwes": [
        1321
      ],
```
