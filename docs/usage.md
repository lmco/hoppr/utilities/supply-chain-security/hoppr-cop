# Usage

## CLI

:::note

The first time you run the command each day can be quite slow as it downloads the databases.  Subsequent runs should be much more performant.

:::

![usage](usage.png)

### CLI options

| Argument | Values | Default | Description |
| ---------------- | -------------- | ------- | ------------------------- |
| --format, -f | "table", "html", "cyclone_dx", "linked", "gitlab" | "table" | The report formats to generate |
| --output-dir, -d | PATH to directory | '.' | The directory where reports will be written |
| --base-report-name, -b | TEXT | "hoppr-cop-report" | The base name supplied for the generated reports |
| --os-distro, -o | TEXT | env var: OS_DISTRIBUTION | The operating system distribution; this is important to ensure accurate reporting of OS vulnerabilities from grype. Examples include rhel:8.6 or rocky:9 |
| --download-dbs | None | None | Mode to allow for downloading vulnerabilty databases |
| --offline | None | None | Mode to allow for offline scans |
| --trace, --no-trace | None | --no-trace | Print traceback information on unhandled error |
| --assessment | TEXT | None | Directory path to your analysis.assessment.yml |
| --epss | None | None | Enable EPSS score enhancement |
| --version | None | None | Show HopprCop version information |
| --install-completion | None | None | Install completion for the current shell. |
| --show-completion | None | None | Show completion for the current shell, to copy it or customize the installation. |
| --help,-h | None | None | Show this message and exit. |

## Hoppr Plugin Configuration

| Name | Default | Options | Notes |
| -----| ------- | ------- | -------- |
| output_dir | `self.context.collect_root_dir + "/generic"` | Local directory path | Default leverages the output location for hoppr which allows for any output to be included in the bundle. |
| base_report_name | `hopprcop-vulnerability-results` | String | |
| scanners | `hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner`<br>`hopprcop.grype. grype_scanner.GrypeScanner`<br>`hopprcop.ossindex.oss_index_scanner.OSSIndexScanner`<br> | `hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner`<br>`hopprcop.grype.grype_scanner.GrypeScanner`<br>`hopprcop.ossindex.oss_index_scanner.OSSIndexScanner`<br>`hopprcop.trivy.trivy_scanner.TrivyScanner` | |
| result_formats | `embedded_cyclone_dx_vex` | `embedded_cyclone_dx_vex`<br>`linked_cyclone_dx_vex`<br>`table`<br>`html`<br>`cyclone_dx`<br>`gitlab`| `embedded_cyclone_dx_vex` and `linked_cyclone_dx_vex` are hoppr specific outputs and handle either embedding vulnerability information inside of the hoppr delivered bom or linking to it via a [VEX Bom](https://cyclonedx.org/capabilities/vex). The other options are default hoppr-cop formats. |

### Example Hoppr Transfer File Configuration

```yaml
schemaVersion: v1
kind: Transfer
stages:
  Vulnerability Check:
    plugins:
    - name: "hopprcop.hoppr_plugin.hopprcop_plugin"
      config:
        result_formats:
          - embedded_cyclone_dx_vex
          - html
        scanners:
          - hopprcop.gemnasium.gemnasium_scanner.GemnasiumScanner
          - hopprcop.grype.grype_scanner.GrypeScanner
          - hopprcop.ossindex.oss_index_scanner.OSSIndexScanner
          - hopprcop.trivy.trivy_scanner.TrivyScanner

max_processes: 3
```
