# Analysis Assessment

This feature documents an assessment of impact, including identifying false positives associated with hoppr-cop scan results. To leverage this feature create an `analysis.assessment.yml` file. See [Assessment Document](#assessment-document) for details.

The path to the folder where the `analysis.assessment.yml` file is located, will need to be provided to the `CLI` command as an argument or `Python` module you are executing the hoppr-cop scan from.

## CLI

Set the `--assessment` option to the path of the folder where the `analysis.assessment.yml` file is located. Relative paths can be used.

For example if your `analysis.assessment.yml` file is located in the current working directory `./` is a valid relative path.

![usage](usage.png)

## Python

If your analysis.assessment.yml is located in the directory you are executing the python module from, `./` is a valid relative path.

In your python module add `combined.set_assessment_path("./")` after `combined = CombinedScanner()`

```python
...

combined = CombinedScanner()
combined.set_assessment_path("./") # Makes CombinedScanner load analysis.assessment.yml
combined.set_scanners([GemnasiumScanner(), GrypeScanner(), TrivyScanner, OSSIndexScanner()])

...
```

The above example adds an assessment to the python module [python-module.md](./python-module.md)

## Assessment Document

The assessment of impact and exploitability of vulnerabilities is documented in an `analysis.assessment.yml` file. The values provided map to the [cyclone-dx vulnerabilities_items_analysis](https://cyclonedx.org/docs/1.5/json/#vulnerabilities_items_analysis) schema.

### Required fields

The root element `assessments` is an array of associated assessment details.

#### Each assessment must include

- `package`, `vulnerability`, `state` and `assessmentDate`.
    - package properties are: `type`, `name` and `version`
- If `state` is not `exploitable` or `in_triage`
    - `detail` is required. Additional field requirement details are located below the example.
- If `state` is `not_affected` or `false_positive`
    - `justification` is required

#### Severity adjustments

If the `state` of the assessment is `resolved`, `resolved_with_pedigree`, `false_positive` or `not_affected`, the associated vulnerability severity will be lowered to `low`

If the `state` of the assessment is `exploitable` or `in_triage` the associated vulnerability severity will not be adjusted

## Example

```yaml
assessments:
- package:
    type: pypi
    name: certifi
    version: "2022.6.15"
  assessmentDate: yyyy-mm-dd
  vulnerability: CVE-2023-xxxx
  state: false_positive
  justification: code_not_reachable
  detail: The code associated with vuln cannot be execute in the context of the application
```

## Assessment Field Level Requirements

```yaml
package: required
  type: required
  name: required
  version: required
           Example: "1.2.3"
           Use double quotes.
```

```yaml
assessmentDate: required
  The date the assessment of the impact and exploitability of the vulnerability completed. Use yyyy-mm-dd date format.
```

```yaml
state: required
  Current state of an occurrence of the vulnerability.

  Must be one of the following values:

  resolved
    vulnerability is remediated.
  resolved_with_pedigree
    vulnerability is remediated and evidence of the changes are provided
  exploitable
    direct or indirect exploit of vulnerability is possible.
  in_triage
    vulnerability under investigation.
  false_positive
    vulnerability was falsely identified.
    justification is required if state is set to false_positive
  not_affected
    system not affected by the vulnerability
    justification is required if state is set to not_affected
```

```yaml
justification: required if state=not_affected or false_positive

  Justification, if provided, must be one of the following values:

  code_not_present
    the code was removed or not present
  code_not_reachable
    vulnerable code is not reachable at runtime
  requires_configuration
    requires a setting to be set or unset
  requires_dependency
    requires a dependency that doesn't exist
  requires_environment
    requires a specific environment which is not present
  protected_by_compiler
    requires a compiler flag to be set or unset
  protected_at_runtime
    prevented at runtime
  protected_at_perimeter
    protected by physical, logical, or network perimeter
  protected_by_mitigating_control
    control(s) were implemented or are in place to reduce the likelihood or impact of the vulnerability
```

```yaml
response: optional
  A response from the vendor or project responsible for the affected component.

  If provided, must be one of the following values:

  can_not_fix
  will_not_fix
  update
  rollback
  workaround_available
```

```yaml
detail: required if state is not exploitable

  Include specific details on why the component or service is not impacted by this vulnerability. Include Compensating Controls if used to mitigate vulnerability.
```
