## [1.4.7](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.6...v1.4.7) (2024-12-10)


### Bug Fixes

* add initial 1.6 support and update trivy scan to support syft generated sbom ([80ae410](https://gitlab.com/hoppr/hoppr-cop/commit/80ae410627d82c6a7109e307892b2d61702aaf2f))

## [1.4.6](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.5...v1.4.6) (2024-11-12)


### Bug Fixes

* bumped trivy and grype versions ([d5dbc0a](https://gitlab.com/hoppr/hoppr-cop/commit/d5dbc0a3250ce34b996e298326eeb4eec2cd0952))

## [1.4.5](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.4...v1.4.5) (2024-10-09)


### Bug Fixes

* added support for tar file extraction ([358f434](https://gitlab.com/hoppr/hoppr-cop/commit/358f4343cda65b3048c957ed58b5be58b6c6869b))

## [1.4.4](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.3...v1.4.4) (2024-07-24)


### Bug Fixes

* capture complete stack trace of scanner errors ([0454d18](https://gitlab.com/hoppr/hoppr-cop/commit/0454d18cdaaf4a25e273dd87311f7028cfdbda84))
* **grype:** handle components without versions instead of throwing an error ([f9102b0](https://gitlab.com/hoppr/hoppr-cop/commit/f9102b0415d61dcaae62f0944c3452087cf6937d))
* **oss-index:** filter out purl strings without versions prior to submitting to OSS Index API ([5d5a6cb](https://gitlab.com/hoppr/hoppr-cop/commit/5d5a6cb1c9eb6a42846a6428f232ac49711ffa0f))
* **oss-index:** Support CVSSv4 vectors ([ff86b4a](https://gitlab.com/hoppr/hoppr-cop/commit/ff86b4a73b912f1d35eb77ef8e1df55fcb42ecf8))
* **trivy:** Handle bom_refs that are not purl strings ([040e70a](https://gitlab.com/hoppr/hoppr-cop/commit/040e70a052dbe9920e342d816f1fed469627397b))

## [1.4.3](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.2...v1.4.3) (2024-07-10)


### Bug Fixes

* allow non-semver versions on components ([ae3c720](https://gitlab.com/hoppr/hoppr-cop/commit/ae3c720f62ca6e9d19f0fedd8575087af7bc260e))
* prevent missing justification error when justification is not required ([ddf945e](https://gitlab.com/hoppr/hoppr-cop/commit/ddf945e4172b5a32c99121142056bed3d17a8772))
* support all scanner purl types ([f7d0d38](https://gitlab.com/hoppr/hoppr-cop/commit/f7d0d381eb8b06f21b115a4b5c674b8a217442e7))

## [1.4.2](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.1...v1.4.2) (2024-05-28)


### Bug Fixes

* added cli flags required for newer trivy version ([1b2e190](https://gitlab.com/hoppr/hoppr-cop/commit/1b2e19063eb10188cd57bc30fdd8181ceb82bca5))

## [1.4.1](https://gitlab.com/hoppr/hoppr-cop/compare/v1.4.0...v1.4.1) (2024-05-01)


### Bug Fixes

* added product list for in-toto layout ([8e753ab](https://gitlab.com/hoppr/hoppr-cop/commit/8e753ab4202a3739fa14bdda6b7f79b62a547778))

## [1.4.0](https://gitlab.com/hoppr/hoppr-cop/compare/v1.3.1...v1.4.0) (2024-04-04)


### Features

* implement epss enhancement ([a1c6295](https://gitlab.com/hoppr/hoppr-cop/commit/a1c6295f65c57a583e46a21cc3065b8c745b4aab))
* scanner return type overhaul (eliminate duplicate vulnerabilities) ([04f4718](https://gitlab.com/hoppr/hoppr-cop/commit/04f4718bd2a566bad235b1d4bf8c7abb501c6527))


### Bug Fixes

* pipeline failures and minor type issue ([f787502](https://gitlab.com/hoppr/hoppr-cop/commit/f787502750b6274e0648cf9e113941c48f737cdb))
* updating for ruff finding and updating deps ([1c2a870](https://gitlab.com/hoppr/hoppr-cop/commit/1c2a870f1bfe14a3be1544b6510e023e63762e4a))

## [1.3.1](https://gitlab.com/hoppr/hoppr-cop/compare/v1.3.0...v1.3.1) (2024-02-13)


### Bug Fixes

* adjusting vulnerability fixture to generate more piecemeal ([8826eb9](https://gitlab.com/hoppr/hoppr-cop/commit/8826eb969c0a6ba0b4227aa42652b057fe626b24))
* correct missing offline-mode page link ([d24dead](https://gitlab.com/hoppr/hoppr-cop/commit/d24deade40bed066cfbca97cfdde702293d62059))
* correcting formatting of the assessment documentation ([dddaa13](https://gitlab.com/hoppr/hoppr-cop/commit/dddaa13a363b3f3277431204f9287d8465765612))
* finalalization update ([b0b2833](https://gitlab.com/hoppr/hoppr-cop/commit/b0b2833d45a4b86727add99a92c9bd3bada91696))
* full test set ([e183e7e](https://gitlab.com/hoppr/hoppr-cop/commit/e183e7e04b32cb87051300194b5cc816f070d598))
* initial implementation pass on gitlab report ([951aede](https://gitlab.com/hoppr/hoppr-cop/commit/951aedea97c061b9e112729925ea5da738e5813b))
* large chunk of reporting tests ([9b7a1e5](https://gitlab.com/hoppr/hoppr-cop/commit/9b7a1e51ff130450e504e04d492754d559d23fed))
* remove testing prints ([7097332](https://gitlab.com/hoppr/hoppr-cop/commit/7097332be60bab9a204c0968fa4e6310c4b15b5c))
* resolve sourcery findings ([57c27c0](https://gitlab.com/hoppr/hoppr-cop/commit/57c27c0db43f80c9d650796d1278b65b7cb3c85d))
* restoring accidentally deleted test_plugin ([bd844b8](https://gitlab.com/hoppr/hoppr-cop/commit/bd844b8f8524e13f9bd5e46a83d1725493ad7349))

## [1.3.0](https://gitlab.com/hoppr/hoppr-cop/compare/v1.2.1...v1.3.0) (2024-02-05)


### Features

* add --version flag option, removes required format argument ([aab7afb](https://gitlab.com/hoppr/hoppr-cop/commit/aab7afb7fddf6b8e0e13d71d88c7a419481e6481))
* added support for offline mode ([f57ad5b](https://gitlab.com/hoppr/hoppr-cop/commit/f57ad5b68afa06c4503a8869ecc2b647051bfb8a))
* Implemented process to add an assessment of impact and exploitability of vulnerabilities ([7cfcbba](https://gitlab.com/hoppr/hoppr-cop/commit/7cfcbbace16994936997b4581d339dc3bb2d87bc))


### Bug Fixes

* adding the trivy test resources ([3a634d2](https://gitlab.com/hoppr/hoppr-cop/commit/3a634d22bd18a54f9d216c77825f3e81bd766164))
* addressing MR threads ([e2d1bc4](https://gitlab.com/hoppr/hoppr-cop/commit/e2d1bc429ee5d374203b4faed256e4346433395d))
* correct url ([fae37e7](https://gitlab.com/hoppr/hoppr-cop/commit/fae37e7ddcd40fd5a4b1c4c57925eee8369aa912))
* correcting a test failure ([afed9de](https://gitlab.com/hoppr/hoppr-cop/commit/afed9de0dfb027da3c828eca16203ed605280701))
* correcting missed formatting and quality gates ([48d2be7](https://gitlab.com/hoppr/hoppr-cop/commit/48d2be7a20b9222f64be0ba169671083e79c65b7))
* duplicate tools in vulnerabilities ([56176a0](https://gitlab.com/hoppr/hoppr-cop/commit/56176a0ac5fbba516ddee3dbc5e655d70c889d66))
* fail on exit if hoppr plugin error occurs ([bf8ced3](https://gitlab.com/hoppr/hoppr-cop/commit/bf8ced315951182969c355e1df1aa446ea61b4ba))
* first pass trivy_scanner reorder ([45fcb5f](https://gitlab.com/hoppr/hoppr-cop/commit/45fcb5fdf15980069f024b73f03ab865cb5fdfe0))
* gitlab report output file format ([29c6aa5](https://gitlab.com/hoppr/hoppr-cop/commit/29c6aa5b3bbb95ffd9ef3efbb63e933c07babdf2))
* incorrect param order for `oss_index_authentication` ([8c81bac](https://gitlab.com/hoppr/hoppr-cop/commit/8c81baca9ff06cedd7642db80eba572b117cfff2))
* initial pass at the trivy scanner tests ([1252baf](https://gitlab.com/hoppr/hoppr-cop/commit/1252baf8f3d8e49e441a89b3a19c2fdff59d3568))
* more refined trivy scanner change ([0a34c3f](https://gitlab.com/hoppr/hoppr-cop/commit/0a34c3fdcc52e4369a2bdf496986590855191c37))
* offline marked as experimental and usage image ([a450d5c](https://gitlab.com/hoppr/hoppr-cop/commit/a450d5c5a08903929e4c62eeb6fa97f656f75576))
* Provide feedback on unsupported purl ([46b65bf](https://gitlab.com/hoppr/hoppr-cop/commit/46b65bfaee693177b8ba0d2f4c7f360c08f7afc0))
* quality gate errors ([5f3b1c0](https://gitlab.com/hoppr/hoppr-cop/commit/5f3b1c0917a144bf990cbb6176a56edbba6a49fe))
* remaining sourcery findings ([66216ec](https://gitlab.com/hoppr/hoppr-cop/commit/66216ecceea3721cbefafbb4e113e2041c6749a3))
* remove sourcery branch job ([d8bbdbd](https://gitlab.com/hoppr/hoppr-cop/commit/d8bbdbd1858d2a81a0767505b95d00f91238953a))
* removing gitlab-test report upload to avoid false positives ([f2db0a8](https://gitlab.com/hoppr/hoppr-cop/commit/f2db0a86e96daaadecb81e533a273ec7a306af27))
* revert `combined_scanner` instance variable change ([412eec3](https://gitlab.com/hoppr/hoppr-cop/commit/412eec34f3f870a81b721580b9d6905da5165061))
* revert some more ([2cc2900](https://gitlab.com/hoppr/hoppr-cop/commit/2cc29004425b9fe24ec75115e1d6c01ca661e348))
* sbom job script to work with version 4+ of cyclonedx-py ([0531848](https://gitlab.com/hoppr/hoppr-cop/commit/0531848e025fb5b0c873d4afea755fc1f0752141))
* type checker findings ([e6b57a1](https://gitlab.com/hoppr/hoppr-cop/commit/e6b57a190cecd93e09c57cb8d3821af60a5008db))
* update gitlab format to match updated schema ([b5ad462](https://gitlab.com/hoppr/hoppr-cop/commit/b5ad462b74fd0eb7d7c33b92fd29849833331e17))
* update gitlab format to match updated schema ([e8c1e13](https://gitlab.com/hoppr/hoppr-cop/commit/e8c1e13c3dffd86831bf3a68bda288a5016812ef))
* update trivy scanner tests to be more complete ([cabc855](https://gitlab.com/hoppr/hoppr-cop/commit/cabc8553dddbae1a389264b84c15aa5261c67ac6))

## [1.2.1](https://gitlab.com/hoppr/hoppr-cop/compare/v1.2.0...v1.2.1) (2023-12-12)


### Bug Fixes

* adjusting the app annotation for entrypoint ([0a354ce](https://gitlab.com/hoppr/hoppr-cop/commit/0a354ce67be8ec7f6e60886af7a73f182057e827))
* correcting the duplication of vulnerabilities in the report ([295b687](https://gitlab.com/hoppr/hoppr-cop/commit/295b68796ae0feba5c6d1c76fe2565eb75b0e9ac))
* ensuring libicu is installed in the docker image for cyclonedx-cli ([cbf8b6e](https://gitlab.com/hoppr/hoppr-cop/commit/cbf8b6ea4173a5ba22c6e4bdc212ea4dc02b993a))
* move libicu72 install to before script ([3e487dc](https://gitlab.com/hoppr/hoppr-cop/commit/3e487dc6c709ebca68e6b90541196689cd2f4a61))
* only add Affect if not already there and create report dir ([d1211a5](https://gitlab.com/hoppr/hoppr-cop/commit/d1211a5b7af9cfc131247da9d8566421b9ddd248))

## [1.2.0](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.22...v1.2.0) (2023-11-30)


### Features

* update to support CDX 1.5 and Grype versions after 0.69.0 ([64f1705](https://gitlab.com/hoppr/hoppr-cop/commit/64f17055f9ad05935ef027c662fece5be4bd135a))

## [1.1.22](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.21...v1.1.22) (2023-11-20)


### Bug Fixes

* update to handle testifysec/witness no longer having releases ([383c294](https://gitlab.com/hoppr/hoppr-cop/commit/383c29423223ccf3dfd5cced968c3084a8cb0d3b))

## [1.1.21](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.20...v1.1.21) (2023-11-02)


### Bug Fixes

*  added ruby, shebang not supported in windows ([9c512ab](https://gitlab.com/hoppr/hoppr-cop/commit/9c512ab85401af63ccbf86b7e1d4960ebf07e9c4))
* affects.ref partition is empty on windows ([af2d481](https://gitlab.com/hoppr/hoppr-cop/commit/af2d481e562969b1222989400ce5af0e3d3f23e7))
* code quality findings ([1d2a303](https://gitlab.com/hoppr/hoppr-cop/commit/1d2a3030aa29ba9d2d5fb0130ca59bbbfa4c5986))
* Go backs ([6d701f6](https://gitlab.com/hoppr/hoppr-cop/commit/6d701f6d4e13a1beab800d49a82473548f1307a1))
* merge branch 'main' of /hoppr/hoppr-cop into issue-26 on fork ([88791cf](https://gitlab.com/hoppr/hoppr-cop/commit/88791cf00f932febdaa072c65cd2a750c533e4a1))
* trivy write to temp exception on windows ([4258b7a](https://gitlab.com/hoppr/hoppr-cop/commit/4258b7a33a602acb1d72568bc897e71d0d1d3170))
* Workaround Gemnasium nuget version spec bug ([52d32ba](https://gitlab.com/hoppr/hoppr-cop/commit/52d32ba3e3cace20c823fc66502e35500e8c6c78))

## [1.1.20](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.19...v1.1.20) (2023-10-23)


### Bug Fixes

* `output_dir` type, `Reporting.output_path` attribute ([6591670](https://gitlab.com/hoppr/hoppr-cop/commit/6591670e554f6b89499d7ba8ffa07e27752ba8aa))

## [1.1.19](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.18...v1.1.19) (2023-10-14)


### Bug Fixes

* **deps:** update dependency aquasecurity/trivy/ to v0.46.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([daf7a87](https://gitlab.com/hoppr/hoppr-cop/commit/daf7a87710cb414fe5524f4267fdb56929af8402))

## [1.1.18](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.17...v1.1.18) (2023-10-12)


### Bug Fixes

* updates to use updated security-commons, linting and formatting fixes ([b83add9](https://gitlab.com/hoppr/hoppr-cop/commit/b83add97f69d0d4a2f0c6177741106c2bbf4a4d2))

## [1.1.17](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.16...v1.1.17) (2023-10-04)


### Bug Fixes

* added cosign login ([29f1373](https://gitlab.com/hoppr/hoppr-cop/commit/29f1373bf71caca11285be8fb427054ae50da38b))

## [1.1.16](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.15...v1.1.16) (2023-10-04)


### Bug Fixes

* corrected image and updated build signing logic ([17d384d](https://gitlab.com/hoppr/hoppr-cop/commit/17d384d0c4ddf1550607c8a93c7b7473991ae53c))

## [1.1.15](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.14...v1.1.15) (2023-10-04)


### Bug Fixes

* added witness install and fixed quotes ([5cc52f1](https://gitlab.com/hoppr/hoppr-cop/commit/5cc52f1b0287b80ed770a894512c8beb8be834cb))

## [1.1.14](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.13...v1.1.14) (2023-10-04)


### Bug Fixes

* added attestation generation and signing ([bf9b368](https://gitlab.com/hoppr/hoppr-cop/commit/bf9b368359a5f9e8cc3d5f4f049d1e883c7b8002))

## [1.1.13](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.12...v1.1.13) (2023-10-04)


### Bug Fixes

* Fix OSS Scanner bug if CWE data contains "noinfo" ([27121e8](https://gitlab.com/hoppr/hoppr-cop/commit/27121e8ca7f857679c0ed4b80c7a9ec1f7923624))

## [1.1.12](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.11...v1.1.12) (2023-09-20)


### Bug Fixes

* **deps:** update dependency aquasecurity/trivy/ to v0.45.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d17cc10](https://gitlab.com/hoppr/hoppr-cop/commit/d17cc10d40059b1f0484e5ca93de271039b9fbad))

## [1.1.11](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.10...v1.1.11) (2023-09-20)


### Bug Fixes

* Dockerfile ARG scoping ([e710764](https://gitlab.com/hoppr/hoppr-cop/commit/e7107642aed5e8760f5e76d3777db38735d9c97f))
* remove references to `Severity` model ([8875fcb](https://gitlab.com/hoppr/hoppr-cop/commit/8875fcbc7b223a75ecbbb330b6fca69ec8ca8d62))

## [1.1.10](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.9...v1.1.10) (2023-08-15)


### Bug Fixes

* support 1.5 spec and custom root models ([f3d8373](https://gitlab.com/hoppr/hoppr-cop/commit/f3d8373c7d87b11c02e131f88116ce9002a54f4e))
* TrivyScanner exception ([7f8fa5d](https://gitlab.com/hoppr/hoppr-cop/commit/7f8fa5dd940421c2b777e2d464deff2681ae8b98))


### Reverts

* testing changes ([d1ae483](https://gitlab.com/hoppr/hoppr-cop/commit/d1ae4833f3519a0d5ad3b637e67644c7f5fb8a43))

## [1.1.9](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.8...v1.1.9) (2023-07-13)


### Bug Fixes

* fixing hoppr plugin load issue with pyproject.toml and security commons dependency issue ([1e6e099](https://gitlab.com/hoppr/hoppr-cop/commit/1e6e0997b448a763f1a38f33f374f827bd7e0f1a))

## [1.1.8](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.7...v1.1.8) (2023-07-13)


### Bug Fixes

* remove purl type filter from HopprCopPlugin (let scanners filter if they need to) ([cf84328](https://gitlab.com/hoppr/hoppr-cop/commit/cf84328f043dc315f4be9b6024c8c7d9f398a09e))
* typos - pretty sure "connan" should be "conan" ([2b0fe21](https://gitlab.com/hoppr/hoppr-cop/commit/2b0fe21957338d3717a475332803d03add7b86ff))

## [1.1.7](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.6...v1.1.7) (2023-06-01)


### Bug Fixes

* added flush to write file contents to disk ([808d2df](https://gitlab.com/hoppr/hoppr-cop/commit/808d2df0f184839881d38ee3c159d01b425213a3))

## [1.1.6](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.5...v1.1.6) (2023-05-16)


### Bug Fixes

* removing npm config that was causing issues. ([f33a66d](https://gitlab.com/hoppr/hoppr-cop/commit/f33a66d34282b60a8a6a67a7e8a1c529f90d8143))

## [1.1.5](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.4...v1.1.5) (2023-04-11)


### Bug Fixes

* Add poetry lockfile back in ([1274cba](https://gitlab.com/hoppr/hoppr-cop/commit/1274cba1b5b050e70eeaec8b301087d6e708570d))

## [1.1.4](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.3...v1.1.4) (2023-04-04)


### Reverts

* add git to test image until 1.8.0 release ([d36b214](https://gitlab.com/hoppr/hoppr-cop/commit/d36b21459b0f6a288f86fcae944ea2534ebde73b))

## [1.1.3](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.2...v1.1.3) (2023-03-20)


### Bug Fixes

* fixing grype model issues where required fields were sometimes blank in grype output. ([71b55a4](https://gitlab.com/hoppr/hoppr-cop/commit/71b55a43be2dc29aaebb68725bc065e02f0c5c4a))

## [1.1.2](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.1...v1.1.2) (2023-03-20)


### Bug Fixes

* fixed debian and rpm reporting for trivy. ([4755549](https://gitlab.com/hoppr/hoppr-cop/commit/4755549d8b45d8348fabe17b79723b8d8a0a25f6))

## [1.1.1](https://gitlab.com/hoppr/hoppr-cop/compare/v1.1.0...v1.1.1) (2023-03-09)


### Bug Fixes

* normalized purl mapping ([7a3d449](https://gitlab.com/hoppr/hoppr-cop/commit/7a3d449920f541df810942f826d872dce244e45c))

## [1.1.0](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.24...v1.1.0) (2023-03-02)


### Features

* adding docker image ([291cca6](https://gitlab.com/hoppr/hoppr-cop/commit/291cca6e734cd371db8c135ac15e250cee8dd9d9))


### Bug Fixes

* updated docker file to include gem install, got gemnasium caching working, added broad catch to cli ([8fbe34a](https://gitlab.com/hoppr/hoppr-cop/commit/8fbe34a791980a509083d93e0bd003eacddeee6c))

## [1.0.24](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.23...v1.0.24) (2023-02-28)


### Bug Fixes

* exposed the ability to specify os distro via the cli or environment variable. This impacts grype's ability to identify vulnerabilities for OS components. ([5f71fc2](https://gitlab.com/hoppr/hoppr-cop/commit/5f71fc2581813bd6638179f56b2bb90d41f954de))

## [1.0.23](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.22...v1.0.23) (2023-02-28)


### Bug Fixes

* the bom generation for trivy was using pydantic and bom-ref was converted to bom_ref. This caused trivy to not report vulnerabilities. This updates the bom generation to just directly go from the dictionary to json. ([c82851a](https://gitlab.com/hoppr/hoppr-cop/commit/c82851a11c7a6ef311b5ad4474edd5fbc1fc8740))

## [1.0.22](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.21...v1.0.22) (2023-02-22)


### Bug Fixes

* Update everything in pyproject.toml to work with upstream dependencies ([fa594c3](https://gitlab.com/hoppr/hoppr-cop/commit/fa594c31b015f56ecc68d132721b605d337726c9))
* Update renovate ([1bec628](https://gitlab.com/hoppr/hoppr-cop/commit/1bec6285fddb69e4e108374482c42d527bf7db8d))
* Update renovate json per linter ([106df54](https://gitlab.com/hoppr/hoppr-cop/commit/106df5416fed3af5638f7b5f7a85830dbd843371))

## [1.0.21](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.20...v1.0.21) (2023-02-09)


### Bug Fixes

* cleaned up pre stage process method and accounting for existing vex ([0b077e5](https://gitlab.com/hoppr/hoppr-cop/commit/0b077e5f7e22e2b594820f58fe476c6585d6ea0b))
* updated plugin to update delivered bom ([5de6f52](https://gitlab.com/hoppr/hoppr-cop/commit/5de6f52422deae7d460b11269f42866d9a6e53f8))

## [1.0.20](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.19...v1.0.20) (2023-01-26)


### Bug Fixes

* adding `poetry lock --no-update` to support renovate MRs. ([9ff7498](https://gitlab.com/hoppr/hoppr-cop/commit/9ff7498e9fb7be643920ac1089366cb8e43267d5))
* update unit test ([0145ba7](https://gitlab.com/hoppr/hoppr-cop/commit/0145ba7170a6c048388a9ced9f57cdeba6f0b7fd))
* updated hoppr version ([020ec43](https://gitlab.com/hoppr/hoppr-cop/commit/020ec4396a64f59b5c912a5b6e19a069a1c87492))

## [1.0.19](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.18...v1.0.19) (2023-01-25)


### Bug Fixes

* **deps:** updating commons version to include gitlab reporting fix ([c6c1c31](https://gitlab.com/hoppr/hoppr-cop/commit/c6c1c31f713509ae85a2aa5ae80a4bddd49ccf29))

## [1.0.18](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.17...v1.0.18) (2023-01-18)


### Bug Fixes

* add integration test job ([c5dea95](https://gitlab.com/hoppr/hoppr-cop/commit/c5dea95f921126a899dba513a594b8cdd15f99af))
* add plugin unit tests ([55d4625](https://gitlab.com/hoppr/hoppr-cop/commit/55d4625d36c29cc2af8e03f733d8800a412298e2))
* **plugin:** added hoppr plugin ([ec3105f](https://gitlab.com/hoppr/hoppr-cop/commit/ec3105f8943e5b499e6db36fc64b47d22036819b))

## [1.0.17](https://gitlab.com/hoppr/hoppr-cop/compare/v1.0.16...v1.0.17) (2022-12-16)


### Bug Fixes

* Bot label for renovate ([3b835f5](https://gitlab.com/hoppr/hoppr-cop/commit/3b835f59e33b267b5070cbb3ccf506d6a5885c41))
* hoppr-security-commons deps ([8feeff6](https://gitlab.com/hoppr/hoppr-cop/commit/8feeff66b3b2622b9de3bb22f5a8ee6150a1e7a1))
* Set renovate config ([fa0de59](https://gitlab.com/hoppr/hoppr-cop/commit/fa0de594f06652c98a4efe238542941bf35c8848))
* updated gitlab namespace ([18968a5](https://gitlab.com/hoppr/hoppr-cop/commit/18968a525cf7559fc8bcf146c9886ebdbc6a2e27))

## [1.0.16](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.15...v1.0.16) (2022-10-19)


### Bug Fixes

* fixed an issue where get_vulnerabilities_by_purl was calling get_vulnerabilities_by_sbom on the underlying scanners ([e5ae0d6](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/e5ae0d600a0b9f8910ff360320eb64143b3b9ab9))

## [1.0.15](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.14...v1.0.15) (2022-10-19)


### Bug Fixes

* remove sbom before regenerating it ([4ed0470](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/4ed0470620db9ae01e5c4b081a0427e67444d951))
* removing references to internal LM resources ([31e112e](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/31e112ebc38139f1b74ae392a29c49acf5f91454))

## [1.0.14](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.13...v1.0.14) (2022-08-29)


### Bug Fixes

* updating gitlab semantic release version ([a1b4685](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/a1b4685907c450502fa826df99b4a87e10f3b147))

## [1.0.13](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.12...v1.0.13) (2022-08-28)


### Bug Fixes

* trying to get release artifacts working ([cfbf1cd](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/cfbf1cd6b50190bac8d096a66a8c985f7c223892))

## [1.0.12](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.11...v1.0.12) (2022-08-28)


### Bug Fixes

* Adding license and sbom to release artifacts ([70c5960](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/70c59603e17baaea63694eee676fdb93fd9f308c))

## [1.0.11](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.10...v1.0.11) (2022-08-27)

## [1.0.10](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.9...v1.0.10) (2022-08-18)

## [1.0.9](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.8...v1.0.9) (2022-08-18)

## [1.0.8](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.7...v1.0.8) (2022-08-18)

## [1.0.7](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.6...v1.0.7) (2022-08-18)


### Bug Fixes

* fixed documentation ([75b12c7](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/75b12c736979fd84c91ecf459dcf4d6f0bf03199))

## [1.0.6](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.5...v1.0.6) (2022-08-18)

## [1.0.5](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.4...v1.0.5) (2022-08-18)

## [1.0.4](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.3...v1.0.4) (2022-08-18)


### Bug Fixes

* added license to the project metadata file ([18fb87f](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/18fb87f073996d3157cb12b8bcaf7c8cd734df91))

## [1.0.3](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.2...v1.0.3) (2022-08-18)

## [1.0.2](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.1...v1.0.2) (2022-08-17)

## [1.0.1](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/compare/v1.0.0...v1.0.1) (2022-08-17)


### Bug Fixes

* changed to use common python module.  Added examples to the documentation. ([b0560f5](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/b0560f5f0300e1c8fc585f1a0bc0e764f48d7806))
* changed to use common python module.  Added examples to the documentation. ([2747d2b](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/2747d2bf1254185c5356f3a67769c5abba00c322))

## 1.0.0 (2022-08-17)


### Features

* added file headers removed extra files, cleaned up docs. ([be06039](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/be060391ac6d22bf0b093fc442550a51f7d20a03))
* use the vex format from grype ([f3d1306](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/f3d13069dfe8f6979d4cff8335ef3f6820faa4cf))


### Bug Fixes

* fixing pipeline issues ([f6b6e51](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/f6b6e51232c8b22a89c7149306a187b1882ab96f))
* fixing semantic release ([abfeefd](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/abfeefd1cff350de63056d973074a537c98837cb))
* linting issues ([585125f](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/585125f1022793e807a32455828483c58f1a7809))
* moved packages around and fixed ci issues ([0e58a3d](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/0e58a3d15d6a39a04be5ba7a2eac8820076ce635))
* updated gemnasium to work properly with the comunity datasource by default, and to update every 24 hours. ([11dea2a](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop/commit/11dea2aa3ce28c31f0aa97bead3686071ffafbec))
